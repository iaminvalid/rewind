﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class AnimationAssistant_Editor : EditorWindow {

	bool groupEnabled;
	float dist;
	float walkTime;
	float runTime;

	GameObject objectA;
	GameObject objectB;

	Node nodeA;
	Node nodeB;

	string animNodeB;

	Vector3 aPos;
	Vector3 bPos;

	// Add menu named "My Window" to the Window menu
	[MenuItem("Tools/Animation Assistant")]
	static void Init()
	{
		// Get existing open window or if none, make a new one:
		AnimationAssistant_Editor window = (AnimationAssistant_Editor)EditorWindow.GetWindow(typeof(AnimationAssistant_Editor));
		window.Show();
	}

	void OnGUI()
	{
		if (objectA != null) 
			GUILayout.Label (objectA.transform.parent.name, EditorStyles.largeLabel);
		
		GUILayout.Label ("Nodes", EditorStyles.boldLabel);

		if (objectA != null)
			objectA = EditorGUILayout.ObjectField ("Object A", objectA, typeof(GameObject), true) as GameObject;
		if (objectB != null)
			objectB = EditorGUILayout.ObjectField ("Object B", objectB, typeof(GameObject), true) as GameObject;

		EditorGUILayout.Space ();
		GUILayout.Label ("Animations", EditorStyles.boldLabel);
		animNodeB = EditorGUILayout.TextField ("Anim A to B", animNodeB);


		if (nodeB!= null && animNodeB != nodeB.stateName) {
			if (GUILayout.Button ("Save Animations")) {
				OverwriteAnimStates ();
			}
		}

		EditorGUILayout.Space ();
		if (GUILayout.Button ("Get Current Node")) {
			GetCurrentNode ();
		}
		if (GUILayout.Button ("Get Next Node")) {
			GetNextNode ();
		}
		if (GUILayout.Button ("<-- Nodes")) {
			PrevNodes ();
		}
		if (GUILayout.Button ("Nodes -->")) {
			NextNodes ();
		}

		EditorGUILayout.Space ();
		dist = EditorGUILayout.FloatField ("Distance", dist);
		EditorGUILayout.Space ();

		if (objectA.transform.position != aPos || objectB.transform.position != bPos) {
			GetDistance ();
		}

		if (GUILayout.Button ("Walk to B")) {
			GetDistance ();
			SetWalking ();
		}
		if (GUILayout.Button ("Run to B")) {
			GetDistance ();
			SetRunning ();
		}

		if (GUI.changed)
			UpdateNodeComponents ();
		

	}

	void GetCurrentNode()
	{
		objectA = Selection.activeGameObject;
		objectB = null;
		UpdateNodeComponents ();
	}
	void GetNextNode()
	{
		objectB = objectA.transform.parent.GetChild (objectA.transform.GetSiblingIndex () + 1).gameObject;
		UpdateNodeComponents ();
	}
	void NextNodes()
	{
		objectA = objectB;
		objectB = objectB.transform.parent.GetChild (objectB.transform.GetSiblingIndex () + 1).gameObject;
		UpdateNodeComponents ();
		Selection.activeObject = objectA;

	}

	void PrevNodes()
	{
		objectB = objectA;
		objectA = objectA.transform.parent.GetChild (objectA.transform.GetSiblingIndex () - 1).gameObject;
		UpdateNodeComponents ();
		Selection.activeObject = objectA;


	}

	void GetDistance()
	{
		if (objectA != null && objectB != null) {
			dist = Vector3.Distance (objectA.transform.position, objectB.transform.position);
			aPos = objectA.transform.position;
			bPos = objectB.transform.position;
		}
	}

	void UpdateNodeComponents()
	{
		nodeA = objectA.GetComponent<Node> () as Node;
		nodeB = objectB.GetComponent<Node> () as Node;
		UpdateAnimStates ();
	}


	void UpdateAnimStates() // Called from UpdateNodeComponents
	{
		if (nodeB != null)
			animNodeB = nodeB.stateName;
	}

	void OverwriteAnimStates()
	{
		if (nodeB != null)
		nodeB.stateName = animNodeB;
	}

	void SetWalking()
	{
		walkTime = dist * 0.8f;
		if (objectA.GetComponent<WaitNode>())
			nodeB.arriveTime = objectA.GetComponent<WaitNode> ().exitWaitTime + walkTime;
		else
			nodeB.arriveTime = objectA.GetComponent<Node> ().arriveTime + walkTime;

		nodeB.stateName = "Walking";
	}

	void SetRunning()
	{
		runTime = dist * 0.3f;
		if (objectA.GetComponent<WaitNode>())
			nodeB.arriveTime = objectA.GetComponent<WaitNode> ().exitWaitTime + runTime;
		else
			nodeB.arriveTime = objectA.GetComponent<Node> ().arriveTime + runTime;

		nodeB.stateName = "Running";
	}
}