﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagicItem : MonoBehaviour {
    public GameObject UICounterPart;
    private bool isOwned;
	public AudioManager audioSettings;
    public PuzzlePiece puzzlePiece;
    // Use this for initialization
	void Start () {
		audioSettings = AudioManager.FindObjectOfType (typeof(AudioManager)) as AudioManager;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void PickUp()
    {
		audioSettings.PlayMagicItemPickup ();
        isOwned = true;
        UICounterPart.SetActive(true);
        gameObject.SetActive(false);
        if (GetComponentInParent<SetActiveTime>() != null)
            GetComponentInParent<SetActiveTime>().isRunning = false;

        puzzlePiece.CheckParticle();
        
    }
    public bool GetOwned()
    {
        return isOwned;
    }
}
