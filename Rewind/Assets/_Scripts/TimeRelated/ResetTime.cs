﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetTime : MonoBehaviour {
    public float resetDuration;
    private float resetTime;
    private float timer;
    private bool resetting;
    private TimeManager timeManager;
	// Use this for initialization
	void Start () {
        timeManager = GameObject.FindGameObjectWithTag("TimeManager").GetComponent<TimeManager>();
        resetting = false;
	}
	
	// Update is called once per frame
	void Update () {
        if (resetting)
        {
            timeManager.scrubBar.value = (timer / resetDuration) * resetTime;
            timer -= Time.unscaledDeltaTime;
            if (timer < 0)
            {
                resetting = false;
            }
        }
	}
    public void RestartTime()
    {
        timer = resetDuration;
        resetting = true;
        resetTime = timeManager.curTime;
    }
}
