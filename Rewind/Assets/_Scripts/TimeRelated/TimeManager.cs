﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeManager : MonoBehaviour {
    public Slider timeSpeedSlider;
    public Slider scrubBar;
    public Text timeDisplay;
    public float curTime;
    public float maxTime;
    public float ResetDuration;
    public float startTime;
    public bool reverseTime;
	public bool skippingToEnd; // Set manually from SkipToEnd Script to stop vignette bug
    private float timeScale;
    private bool UpdateTime = true;

    VignetteControl camEffect;
    CutScene cutsceneManager;


    // Use this for initialization
    void Start() {
        scrubBar.maxValue = maxTime;
        scrubBar.value = startTime;
        camEffect = Camera.main.GetComponent<VignetteControl>();
        cutsceneManager = GameObject.Find("CutSceneManager").GetComponent<CutScene>();
    }

    // Update is called once per frame
    void Update() {

		curTime = scrubBar.value;

        if (!UpdateTime)
        {
            //StartUpdate();

            if (Input.GetMouseButtonUp(0))
                StartUpdate();
            return;

        }
        
			if (Input.GetKey(KeyCode.LeftArrow))
			{
				timeSpeedSlider.value -= 2 * Time.unscaledDeltaTime;
			}
			if (Input.GetKey(KeyCode.RightArrow))
			{
				timeSpeedSlider.value += 2 * Time.unscaledDeltaTime;
			}

            timeScale = timeSpeedSlider.value;

        if (scrubBar.value >= maxTime && timeSpeedSlider.value > 0)
            timeScale = 0;

		if (!skippingToEnd && curTime == maxTime)
			camEffect.StartEffect ();
		if (skippingToEnd && curTime != maxTime)
			camEffect.EndEffect ();
		
		else if (curTime < maxTime - 0.1f && curTime > maxTime - 0.5f && reverseTime)
			camEffect.EndEffect ();
		



            Time.timeScale = Mathf.Abs(timeScale);

            if (timeScale < 0)
            {
                scrubBar.value -= Time.deltaTime;
                if (curTime <= 0)
                    curTime = 0;
                
                reverseTime = true;
            }
            else 
            {
                scrubBar.value += Time.deltaTime;
                reverseTime = false;
            }

        curTime = scrubBar.value;

     //   timeDisplay.text = curTime.ToString();
    }


    public float AdjustFloat(float floatToAdjust)
    {
        if (!reverseTime)
            return floatToAdjust;
        else
            return -floatToAdjust;
    }

    public void finishGame()
    {
        timeSpeedSlider.gameObject.SetActive(false);
        GameObject.Find("TimeSpin").SetActive(false);
    }
    public void PauseUpdate()
    {
        UpdateTime = false;
    }
    public void StartUpdate()
    {

            UpdateTime = true;
        
    }
    public void ResetTime()
    {

        curTime = 0;
    }
    public bool GetUpdate()
    {
        return UpdateTime;
    }
}
