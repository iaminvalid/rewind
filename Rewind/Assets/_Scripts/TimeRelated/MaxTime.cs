﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MaxTime : MonoBehaviour {
    public float UnfinishedMaxTime;
    public float midMaxTime;
    public float FinishedMaxTime;
    public Slider scrubBar;

    public PuzzlePiece finalPieceJewellery;
    public FinalPuzzlePiece finalPieceFlowers;
    public PuzzlePiece[] finalPieceList;
    private TimeManager timeManager;

    public enum finalTimeEnum
    {
        unfinishedMaxTime,
        midMaxTime,
        finishedMaxTime
    }
    public finalTimeEnum maxTimeSetting;

    
    // Use this for initialization
	void Start () {
        timeManager = GameObject.FindGameObjectWithTag("TimeManager").GetComponent<TimeManager>();
        maxTimeSetting = finalTimeEnum.unfinishedMaxTime;
	}
	
	// Update is called once per frame
	void Update () {

        //   for (int i = 0; i < finalPieceList.Length; i++)
        //   {
        //       if (finalPieceList[i].isSelected == false)
        //       {
        //           timeManager.maxTime = UnfinishedMaxTime;
        //           return;
        //       }
        //       
        //   }


		// Rushed code. Should probably be called from PuzzlePiece on completion, and not in Update
        if (finalPieceJewellery.isSelected == false && finalPieceFlowers.isSelected == true)
            maxTimeSetting = finalTimeEnum.midMaxTime;
        else if (finalPieceJewellery.isSelected == true && finalPieceFlowers.isSelected == true)
            maxTimeSetting = finalTimeEnum.finishedMaxTime;
        else
            maxTimeSetting = finalTimeEnum.unfinishedMaxTime;

        timeManager.maxTime = SetMaxTime();
        scrubBar.maxValue = SetMaxTime();

	}

    float SetMaxTime()
    {
        switch (maxTimeSetting)
        {
            case finalTimeEnum.unfinishedMaxTime:
                return UnfinishedMaxTime;
               
            case finalTimeEnum.midMaxTime:
                return midMaxTime;
               
            case finalTimeEnum.finishedMaxTime:
                return FinishedMaxTime;
                
        }
        return UnfinishedMaxTime;
    }
}
