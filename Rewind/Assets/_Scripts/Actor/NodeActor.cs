﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NodeActor :  BaseActor{

    public GameObject StartNode;
    private GameObject currentObject;
    private Node currentNode;
    private GameObject lookAtObject;
    private string currentAnimation;

    private AnimationClip anim;
	// Use this for initialization
	void Start () {


        

        
        
        currentObject = StartNode;
        currentNode = StartNode.GetComponent<Node>();

        timeManager = GameObject.FindGameObjectWithTag("TimeManager").GetComponent<TimeManager>();
        startPos = gameObject.GetComponent<StartPosition>();
        transform.LookAt(StartNode.transform);
        startPos.forward = transform.forward;

        StartNode.GetComponent<Node>().SetStartPos(GetComponent<StartPosition>().position);

        currentNode = currentNode.transitionNode(timeManager.curTime).GetComponent<Node>();
        

    }
	
    
	// Update is called once per frame
	override public void Update () {


        if (actorAnimator != null)
            UpdateAnimation();
        if (timeManager.curTime < 0)
            return;

        transform.position = currentNode.Move(timeManager.curTime, transform.position);
        if (currentNode.GetAnimationState() != currentAnimation)
        {
            currentAnimation = currentNode.GetAnimationState();
            if (timeManager.curTime != 0 || currentNode.GetArriveTime() != 0)
            {
            float normalizedTime =  Mathf.Abs(timeManager.scrubBar.value - currentNode.GetPreviousArriveTime()) / (currentNode.GetArriveTime() - currentNode.GetPreviousArriveTime());
            actorAnimator.Play(currentAnimation,-1,normalizedTime);

            }

        }

        if (!timeManager.GetUpdate() && timeManager.scrubBar.value != 0)
        {
            float normalizedTime = (((Mathf.Abs(timeManager.scrubBar.value - currentNode.GetPreviousArriveTime()) % 2.7f) / ((currentNode.GetArriveTime() - currentNode.GetPreviousArriveTime())%2.7f)) ) ;
            actorAnimator.Play(currentAnimation, -1, normalizedTime);

        }



        //transform.LookAt(currentNode.NextNode.transform);
        if (currentNode.transitionNode(timeManager.curTime) != currentObject)
        {
            while (currentNode.transitionNode(timeManager.curTime) != currentObject)
            {

                currentObject = currentNode.transitionNode(timeManager.curTime);
                currentNode = currentObject.GetComponent<Node>();

            }
        }

        if (!currentNode.isFirstNode)
            transform.forward = currentNode.GetDirection();
        else
            transform.forward = startPos.forward;


    }
    protected override void UpdateAnimation()
    {
		if (actorAnimator == null)
			return;

        base.UpdateAnimation();

		}
    //public  void addWaitNode(float duration)
    // {
    //
    //     timeManager.maxTime += duration;
    //
    //     GameObject tempnode = Instantiate(NodePrefab,curNode.transform.parent);
    //     tempnode.transform.position = transform.position;
    //     tempnode.GetComponent<WaitNode>().waitDuration= duration;
    //     curNode.GetComponent<Node>().NewNode(tempnode,duration,timeManager.curTime );
    //     curNode = tempnode;
    // }

}
