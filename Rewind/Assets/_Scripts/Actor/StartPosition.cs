﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartPosition : MonoBehaviour {
    public Vector3 position;
    public Vector3 forward;
    private Vector3 tempPos;
    private Quaternion rotation;
    private Quaternion tempRot;

    private TimeManager anim;
	// Use this for initialization
	void Awake () {
             
        position = transform.position;
        rotation = transform.rotation;
        anim = GameObject.FindGameObjectWithTag("TimeManager").GetComponent<TimeManager>();

	}
	
	// Update is called once per frame
	void Update () {

        if (anim.curTime < 0)
        {

            transform.position = Vector3.Lerp(tempPos, position,    1 - Mathf.Abs(anim.curTime / anim.ResetDuration) );
            transform.rotation = Quaternion.Lerp(tempRot, rotation, 1 - Mathf.Abs(anim.curTime / anim.ResetDuration) );
        }
        else
        {
            tempPos = transform.position;
            tempRot = transform.rotation;
        }
	}
}
