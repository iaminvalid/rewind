﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CutScene : MonoBehaviour {

    public GameObject UICanvas;
    public GameObject cinematicCanvas;

    public GameObject mainCamera;
    public GameObject otherCamera;
    public GameObject TopCinematicBar;
    public GameObject BottomCinematicBar;

    public GameObject tutorialManager;

    public Quaternion targetRotation;
	[Space]
	public AudioSource gameMusicAudio;
	public AudioSource momentMusicAudio;
	public AudioSource carCrashAudio;
	public AudioSource cutsceneRewindAudio;
	bool audioFired;

	[Space]
	public Animator titleLogo;


    public float rewindDuration;
    public float cutSceneEndTime;

    private GameObject cameraPivot;
    private float timer;
    private bool activeGame = false;
    private bool rewind = false;
    private Quaternion startRotation;
    private TimeManager timeManager;
    // Use this for initialization
	void Start () {
		titleLogo.gameObject.SetActive (false);
        cameraPivot = otherCamera.transform.parent.parent.gameObject;
        startRotation = cameraPivot.transform.rotation;
        timeManager = GameObject.FindGameObjectWithTag("TimeManager").GetComponent<TimeManager>();
        UICanvas.SetActive(false);
        otherCamera.GetComponent<InputScript>().enabled = false;
//		mainCamera.GetComponent<InputScript>().enabled = false;
        
    }
	
	// Update is called once per frame
	void Update () {

		//Play car sound only once
		if (carCrashAudio.isPlaying == false && audioFired == false && timeManager.curTime >= cutSceneEndTime - 1)
		{
			carCrashAudio.Play ();
			audioFired = true;
		}

        if (timeManager.curTime >= cutSceneEndTime && !rewind)
        {
			StartCoroutine (CarCrashEffects ());
        }

        if (rewind)
        {
            timer += Time.unscaledDeltaTime;
            timeManager.scrubBar.value = Mathf.Lerp(cutSceneEndTime, 0, timer / rewindDuration);
            cameraPivot.transform.rotation = Quaternion.Slerp(startRotation, targetRotation, timer / rewindDuration);

            if ((rewindDuration - timer) <= 1)
            {
                TopCinematicBar.GetComponent<RectTransform>().position += Vector3.up * Time.unscaledDeltaTime * 150;
                BottomCinematicBar.GetComponent<RectTransform>().position -= Vector3.up * Time.unscaledDeltaTime * 150;

            }
            if (timer >= rewindDuration)
            {
                EndCutScene();
            }
        }

        if (Input.GetKey(KeyCode.Z) && Input.GetKey(KeyCode.X))
        {
            forceEndCutScene();
        }

	}

	IEnumerator CarCrashEffects()
	{
		
		Camera.main.transform.GetComponent<VignetteControl> ().StartEffect ();
		GameObject.Find ("TimeManager").GetComponent<TimeManager> ().timeSpeedSlider.value = 0;

		yield return new WaitForSecondsRealtime (2);
		rewind = true;
		//timeManager.PauseUpdate();
		Camera.main.transform.GetComponent<VignetteControl> ().EndEffect ();
		if (cutsceneRewindAudio.isPlaying == false)
			cutsceneRewindAudio.Play ();

	}

    private void EndCutScene()
    {
        cinematicCanvas.SetActive(false);
		timeManager.scrubBar.value = 1;
		timeManager.timeSpeedSlider.value = 1;
        timeManager.StartUpdate();

        UICanvas.SetActive(true);
        otherCamera.GetComponent<InputScript>().enabled = true;
		//mainCamera.GetComponent<InputScript>().enabled = true;

        gameObject.GetComponent<CutScene>().enabled = false;
        tutorialManager.GetComponent<TutorialManager>().ActivateTutorial();
		Camera.main.transform.GetComponent<VignetteControl> ().EndEffect ();

		carCrashAudio.Stop ();
		carCrashAudio.enabled = false;
		cutsceneRewindAudio.Stop ();
		cutsceneRewindAudio.enabled = false;

		gameMusicAudio.Play ();
		momentMusicAudio.Play ();

		titleLogo.gameObject.SetActive (true);
		//titleLogo.Play("ANIM_StartLogoInGame");

        this.enabled = false;
    }
	public void forceEndCutScene()
    {

        cameraPivot.transform.rotation = targetRotation;


        EndCutScene();
    }

}
