﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightmapFix : MonoBehaviour {

    private MeshRenderer myMesh;
    public int lightmapID;

	// Use this for initialization
	void Start () {
        myMesh = GetComponent<MeshRenderer>();
        myMesh.lightmapIndex = lightmapID;
	}
	
}
