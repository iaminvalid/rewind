﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimEventLamp : MonoBehaviour {

	public GameObject itemLightbulb;
	public ParticleSystem fxLightBreak;

    public void LightBreak()
    {
		if(GameObject.FindGameObjectWithTag("TimeManager").GetComponent<TimeManager>().reverseTime)
			itemLightbulb.SetActive (true);
		else
			itemLightbulb.SetActive (false);
			
		fxLightBreak.transform.SetParent (null);
		fxLightBreak.Play();
    }
}
