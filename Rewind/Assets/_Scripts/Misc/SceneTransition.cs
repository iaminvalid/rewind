﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class SceneTransition : MonoBehaviour {
    public string SceneName;
	public GameObject loadingText;
	public bool loadCurrentScene;


    public virtual void NextScene()
    {
        if (SceneName == "Quit")
            Application.Quit();

		if (SceneManager.GetActiveScene().name == "MainMenu")
			loadingText.SetActive (true);

		if (!loadCurrentScene)
			SceneManager.LoadScene (SceneName);
		else
			SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex);

            
    }
}
