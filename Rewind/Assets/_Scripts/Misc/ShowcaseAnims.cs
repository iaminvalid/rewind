﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowcaseAnims : MonoBehaviour {

	Animator myAnimController;
	public enum animEnum{
		walking_F,
		walking_M,
		walking_Bride,
		Thinking,
		Kiss,
		Running,
		Idle,
		Dance,
		Dance2,
		Arguing,
		Arguing2,
		Cheering,
		Talking
	}
	public animEnum animationState;

	animEnum prevState;

	void Start()
	{
		myAnimController = this.GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (prevState != animationState) {
			NewAnim ();
			prevState = animationState;
		}
		
	}

	void NewAnim()
	{
		switch (animationState) {
		case animEnum.walking_F:
			myAnimController.Play ("Walking_F");
			break;
		case animEnum.walking_M:
			myAnimController.Play ("Walking_M");
			break;
		case animEnum.walking_Bride:
			myAnimController.Play ("Walking_Bride");
			break;
		case animEnum.Kiss:
			myAnimController.Play ("Kiss");
			break;
		case animEnum.Running:
			myAnimController.Play ("Running");
			break;
		case animEnum.Thinking:
			myAnimController.Play ("Thinking");
			break;
		case animEnum.Arguing:
			myAnimController.Play ("Arguing");
			break;
		case animEnum.Arguing2:
			myAnimController.Play ("Arguing2");
			break;
		case animEnum.Cheering:
			myAnimController.Play ("Cheering");
			break;
		case animEnum.Dance:
			myAnimController.Play ("Dance");
			break;
		case animEnum.Dance2:
			myAnimController.Play ("Dance2");
			break;
		case animEnum.Idle:
			myAnimController.Play ("Idle");
			break;
		case animEnum.Talking:
			myAnimController.Play ("Talking");
			break;
		}
		return;
	}
}
