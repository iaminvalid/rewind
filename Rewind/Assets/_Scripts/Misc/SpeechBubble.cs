﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeechBubble : MonoBehaviour {
    private TimeManager timeManager;
    public MagicItemPuzzlePiece magicItemPuzzlePiece;
    public GameObject child;
    public bool isHoldingObject;
    public float startTime;
    public float endTime;

	// Use this for initialization
	void Start () {
        timeManager = GameObject.FindGameObjectWithTag("TimeManager").GetComponent<TimeManager>();
	}
	
	// Update is called once per frame
	void Update () {
;
        if (magicItemPuzzlePiece.isSelected)
            return;

        if (timeManager.curTime > startTime && timeManager.curTime < endTime && !isHoldingObject)
            child.SetActive(true);
        else
            child.SetActive(false);
        
	}
}
