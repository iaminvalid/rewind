﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleStartButton : MonoBehaviour {
    public TimeManager timeManager;
    //public PuzzleStore puzzleStore;
    public PuzzleState puzzleState;
    public PuzzlePiece[] solutionArray;

    public float StartTime;
    public float EndTime;

    
    public int PuzzleIndex;

    public bool isclicked;

    public MomentWheelIconSpawner cyclerMomentSpawner;
    public int index;
    private float curTime;
    private bool firstClick = false;

    // Use this for initialization
    void Start () {
        
        timeManager = GameObject.FindGameObjectWithTag("TimeManager").GetComponent<TimeManager>();
      //  puzzleStore = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PuzzleStore>();
        puzzleState = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PuzzleState>();
        isclicked = false;
    }

    // Update is called once per frame
    void Update () {

        curTime = timeManager.curTime;

        //if (curTime >= StartTime && curTime <= EndTime && !isclicked)
        //    transform.Find("ClockTarget").gameObject.SetActive(true) ;
        //else
        //    transform.Find("ClockTarget").gameObject.SetActive(false);
		


    }

    public void UpdatePuzzle(int value)
    {
        //puzzleStore.UpdatePuzzle(PuzzleIndex, value);
        for (int i = 0; i < solutionArray.Length; i++)
        {
            if (solutionArray[i].PuzzleIndex != value)
                solutionArray[i].UpdateSelected(false);
            else
                solutionArray[i].UpdateSelected(true);

        }

            isclicked = false;

            puzzleState.EndPuzzle();   
    }
    public void EndPuzzle()
    {


        isclicked = false;
    }
    public void StartPuzzle()
    {


		if (!firstClick){
			firstClick = true;
			cyclerMomentSpawner.AddIcon(index);
        }


        isclicked = true;
       

    }
}
