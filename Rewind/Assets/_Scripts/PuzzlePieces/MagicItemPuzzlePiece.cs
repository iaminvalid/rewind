﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagicItemPuzzlePiece : PuzzlePiece {
    public Node NodeToChange;
    public GameObject magicItemUI;
    public bool wasTapped;
    private bool canTap;
    public AudioClip successSound;
    public AudioClip failSound;

	void Start()
	{
		audioSettings = AudioManager.FindObjectOfType (typeof(AudioManager)) as AudioManager;
	}

	// Update is called once per frame
    public void CheckCurrentItem(GameObject item)
    {

        if (!startButton.isclicked)
            return;
        if (item == magicItemUI)
        {
            if (!canTap)
            {

                wasTapped = true;

                canTap = true;
                if (NodeToChange != null)

                    NodeToChange.ItemChange(true);

				audioSettings.PlaySolutionSelect ();

                magicItemUI.GetComponent<MagicItemUI>().DropItem();
            }

            Tapped();
            if (wasTapped)
                return;

        }
        else
        {
            Debug.Log("");
            //GetComponent<AudioSource>().clip = failSound;
            //GetComponent<AudioSource>().Play();
        }
        return;
        
    }
    public override  void Tapped()
    {
        
        if (canTap)
        {
            
            base.Tapped();
            canTap = false;
        }

    


    }
    protected override void additionalParticleChecks()
    {
        if (magicItemUI.activeInHierarchy && particleOn)
            particleOn = true;
        else
            particleOn = false;
    }
}
