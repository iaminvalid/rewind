﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class itemStateChange : MonoBehaviour {
    private MagicItemPuzzlePiece magicPiece;
    public GameObject objectToChange;
	// Use this for initialization
	void Start () {
        magicPiece = gameObject.GetComponent<MagicItemPuzzlePiece>();
	}
	
	// Update is called once per frame
	void Update () {
        if (!magicPiece.wasTapped)
            objectToChange.GetComponent<Renderer>().material.color = new Color(0, 255, 0, 1);
        else
            objectToChange.GetComponent<Renderer>().material.color = new Color(255, 0, 0, 1) ;

    }
}
