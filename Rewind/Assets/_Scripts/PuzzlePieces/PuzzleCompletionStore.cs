﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleCompletionStore : MonoBehaviour {
    public int puzzleCount;
    public float[] timeStore;
    public bool[] completionStore;
    public string[] animationName;
    public Animator anim;
    public GameObject mainCharacter;
    private TimeManager timeManager;
    // Use this for initialization
	void Start () {
        timeManager = gameObject.GetComponent<TimeManager>();
        timeStore = new float[puzzleCount];
        completionStore = new bool[puzzleCount];
        
        for (int i = 0; i < puzzleCount; i++)
        {
            timeStore[i] = 0;
            completionStore[i] = false;
        }
	}
	
	// Update is called once per frame
	void Update () {

        for (int i = 0; i < puzzleCount; i++)
        {
            if (completionStore[i])
            {
                if (Mathf.Approximately(timeManager.curTime,timeStore[i]) )
                {
                    anim.SetBool(animationName[i], true);
                }
                if (timeManager.curTime > 2 && timeManager.curTime < 2.5f && !timeManager.reverseTime)
                {
                    //mainCharacter.GetComponent<ForwardActor>().StartWait(1);
                }
                if (timeManager.curTime > 2 + 3 && timeManager.curTime < 2.5f + 3 && timeManager.reverseTime)
                {

                    //mainCharacter.GetComponent<ForwardActor>().StartWait(3);
                }
            }
        }	
	}

    public void CompletePuzzle(int puzzleIndex, float a_time, bool a_complete)
    {
        timeStore[puzzleIndex] = a_time;
        completionStore[puzzleIndex] = a_complete;


    }
}
