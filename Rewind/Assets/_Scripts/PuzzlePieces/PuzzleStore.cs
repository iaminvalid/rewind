﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleStore : MonoBehaviour {
    public int totalPuzzles;
    public int[] KeyCode;
	// Use this for initialization
	void Start () {
        KeyCode = new int[totalPuzzles];
	}
	
	// Update is called once per frame
	void Update () {

	}
    public void UpdatePuzzle(int index, int value)
    {

        KeyCode[index] = value;
    }
}
