﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialPuzzlePiece : PuzzlePiece {
    public TutorialMomentPiece tutorialPiece;

    public override void Tapped()
    {
        if (tutorialPiece.GetTriggered())
            base.Tapped();
        
    }


}
