﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzlePiece : MonoBehaviour {
    public GameObject particle;
    public GameObject icon;
	public GameObject greenCircle;
	public GameObject miniClock;
    public TimeManager timeManager;
    public PuzzleStartButton startButton;
    public PuzzlePiece pastPiece;
    //public PuzzleStore puzzleStore;
    public Animator myAnim;
    public string animationTriggerName;

    public string animationStateName;

    public float animStartTime;
    private float animEndTime;
    private float animLength = .767f;

    public int PuzzleIndex;
    public bool isSelected;
    public bool addMagicItem;
    public GameObject WorldMagicItemPiece;
    public string[] animationBoolsToChange;
    public Animator[] animatorToUpdate;
    public Node[] NodeToUpdate;
    public SetActiveTime[] setActiveList;
    protected bool particleOn;
    private bool updateParticle;
	public AudioManager audioSettings;

	// Use this for initialization
	void Start () {

        timeManager = GameObject.FindGameObjectWithTag("TimeManager").GetComponent<TimeManager>();
		audioSettings = AudioManager.FindObjectOfType (typeof(AudioManager)) as AudioManager;

        

        animEndTime = animStartTime + animLength;
        //particle.GetComponent<ParticleSystem>().Stop(true, ParticleSystemStopBehavior.StopEmittingAndClear);
        TurnOffParticle();
		icon.SetActive (true);
		greenCircle.SetActive (false);
		icon.SetActive (false);

        particleOn = true;
    }
	
	// Update is called once per frame
	protected void Update () {

        UpdateParticle();
        UpdateAnimation();

    }

    public virtual void Tapped()
    {
        if (pastPiece != null)
            if (!pastPiece.isSelected)
                return;

        if (startButton.isclicked)
        {
			audioSettings.PlaySolutionSelect ();
            if (!isSelected)
            {
                startButton.UpdatePuzzle(PuzzleIndex);
				greenCircle.SetActive (true);
                isSelected = true;
            }
            else
            {
                startButton.UpdatePuzzle(-1);
				greenCircle.SetActive (false);
				isSelected = false;
            }

        }

        



    }
    public void UpdateSelected(bool canMove)
    {
        isSelected = canMove;
        if (myAnim != null)
            myAnim.SetBool(animationTriggerName, canMove);
        if (WorldMagicItemPiece != null)
            ChangeMagicItem(canMove);
        UpdateAnimator(canMove);
        
        
    }
    public void UpdateParticle()
    {
        if (particle == null || !updateParticle)
            return;


            particle.GetComponent<ParticleSystem>().Simulate(Time.unscaledDeltaTime,true,false);

    }
    protected virtual void UpdateAnimation()
    {
        if (myAnim == null)
            return;

        myAnim.SetFloat("Time", timeManager.curTime);
        if (timeManager.curTime > animStartTime && timeManager.curTime < animEndTime)
        {



            float normTime = (timeManager.curTime - animStartTime) / animLength;

            myAnim.Play(myAnim.GetCurrentAnimatorStateInfo(0).fullPathHash, 0, normTime);            
                
                
        }
        if (timeManager.curTime > animEndTime || timeManager.curTime < animStartTime)
        {
            float temp = timeManager.curTime > animEndTime ? 1 : 0;

            if (myAnim != null)
            {

                //myAnim.Play(myAnim.GetCurrentAnimatorStateInfo(0).fullPathHash, 0, temp);
            }
        }
    }
    public void TurnOnParticle()
    {
        CheckParticle();
        icon.SetActive(true);
        updateParticle = true;
        miniClock.SetActive(true);
        particle.GetComponent<ParticleSystem>().Play(true);
    }
    protected virtual void additionalParticleChecks()
    {

    }
    public void TurnOffParticle()
    {
        updateParticle = false;

        icon.SetActive(false);
		particleOn = true;
        particle.GetComponent<ParticleSystem>().Stop(true,ParticleSystemStopBehavior.StopEmittingAndClear);

       // particle.SetActive(false);
    }
    public void CheckParticle()
    {
        particleOn = true;

        if (startButton.isclicked)
        {
            miniClock.GetComponent<SpriteRenderer>().color = Color.white; // is Available
        }
        else
            miniClock.GetComponent<SpriteRenderer>().color = new Color(0.3f, .3f, .3f, 1); // is not available

        ParticleSystem.MainModule settings = particle.GetComponent<ParticleSystem>().main;
        if (pastPiece != null)
        {
            if (pastPiece.isSelected)
                particleOn = true;
            else
            {
                particleOn = false;

            }
        }
        if (particleOn && startButton.isclicked)
            particleOn = true;
        else
            particleOn = false;

        additionalParticleChecks();

        if (particleOn)
        {
            icon.GetComponent<SpriteRenderer>().color = Color.white;
            settings.startColor = Color.cyan;
        }
        else
        {
            icon.GetComponent<SpriteRenderer>().color = new Color(0.3f, .3f, .3f, 1);
            settings.startColor = Color.black;
        }
    }
    public void UpdateAnimator(bool valueChange)
    {
        for (int i = 0; i < animatorToUpdate.Length; i++)
            animatorToUpdate[i].SetBool(animationBoolsToChange[i], valueChange);
    
        for (int i = 0; i < NodeToUpdate.Length; i++)
            NodeToUpdate[i].ItemChange(valueChange);
        
        for (int i = 0; i < setActiveList.Length; i++)
            setActiveList[i].UpdateIsRunning(valueChange);
        
    }

    public void ChangeMagicItem(bool value)
    {
        if (WorldMagicItemPiece == null && !isSelected)
            return;
        if (addMagicItem)
        {
            if (value)
                AddMagicItem();
            else
                RemoveMagicItem();

        }
        else
        {
            if (value)
                WorldMagicItemPiece.SetActive(true);
            else
                WorldMagicItemPiece.SetActive(false);

        }

    }
    private void AddMagicItem()
    {
        WorldMagicItemPiece.GetComponent<MagicItem>().PickUp();

		// We're just gonna de-activate the icon here so players don't try to use it again
		icon.SetActive (false);
    }

    private void RemoveMagicItem()
    {
        WorldMagicItemPiece.GetComponent<MagicItem>().UICounterPart.GetComponent<MagicItemUI>().DropItem();
    }
}
