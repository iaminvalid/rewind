﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinalPieceTimer : MonoBehaviour {
    public float displayDuration;
    public GameObject TutorialSkipButton;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        displayDuration -= Time.unscaledDeltaTime;

        if (displayDuration <= 0)
        {
            gameObject.GetComponent<TutorialPiece>().TutorialComplete();
            TutorialSkipButton.SetActive(false);
        }
	}
}
