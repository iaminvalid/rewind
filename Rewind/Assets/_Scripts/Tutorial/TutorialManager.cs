﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialManager : MonoBehaviour {
    public TutorialPiece[] tutorialPieceList;
	private TimeManager timeManager;
    public GameObject tutorialSkipButton;
	public GameObject tutMomentExitButton;
	public PuzzleState puzzleState;
	void Start()
	{
		timeManager = GameObject.FindGameObjectWithTag ("TimeManager").GetComponent<TimeManager> ();
		foreach (TutorialPiece tp in tutorialPieceList)
		{
			tp.gameObject.SetActive (false);
		}
	}


	public void ActivateTutorial() {

            for (int i = 1; i < tutorialPieceList.Length; i++)
            {
                tutorialPieceList[i].SetIndex(i);
                tutorialPieceList[i].tutorialManager = this;
			    tutorialPieceList[i].gameObject.SetActive(false);
            }

			//Turn step 1 on
			tutorialPieceList[0].gameObject.SetActive(true);

        


    }


    public void CompleteTutorialPiece(int index)
    {
		tutorialPieceList[index].gameObject.SetActive(false);
		if (index + 1 < tutorialPieceList.Length)
			tutorialPieceList [index + 1].gameObject.SetActive (true);
		else
			EntireTutorialComplete ();
    }

    public void SkipTutorial()
    {
        for (int i = 0; i < tutorialPieceList.Length; i++)
        {
			tutorialPieceList[i].ForceEnd();
			tutorialPieceList [i].gameObject.SetActive (false);
        }

        tutorialSkipButton.SetActive(false);

	//	timeManager.StartUpdate();

		if (puzzleState.curPuzzle == null) {
		//	timeManager.timeSpeedSlider.value = 1;
			
		}

		timeManager.StartUpdate ();
		timeManager.timeSpeedSlider.value = 0;
    }

	public void EntireTutorialComplete()
	{
		tutorialSkipButton.SetActive(false);
		tutMomentExitButton.GetComponent<Button> ().onClick.Invoke ();
		timeManager.StartUpdate ();
		timeManager.timeSpeedSlider.value = 1;
		timeManager.ResetTime ();
	}


}
