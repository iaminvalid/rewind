﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementTutorialPiece : TutorialPiece {

    public PinPointWorldMovement worldMovement;

    public override void SetIndex(int index)
    {
        base.SetIndex(index);
        worldMovement.SetTutorialMode(true);
    }
    public override void TriggerTutorial()
    {
        base.TriggerTutorial();
        worldMovement.SetTutorialMode(false);

    }
	public override void ForceEnd ()
	{
		worldMovement.SetTutorialMode (false);
		base.ForceEnd ();
	}
	public override void TutorialComplete()
	{
		if (timeManager == null)
			return;
		if (timeManager.curTime >= startTime && !isComplete)
		{
		//	timeManager.StartUpdate();
		//	timeManager.timeSpeedSlider.value = 1;
			tutorialManager.CompleteTutorialPiece(indexPosition);
			isComplete = true;
			textHolder.SetActive(false);
			worldMovement.SetTutorialMode (false);
		}
	}
}
