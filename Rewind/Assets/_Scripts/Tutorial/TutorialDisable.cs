﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialDisable : MonoBehaviour {
    public TutorialPiece tutorialPiece;
	// Use this for initialization
	void Start () {
        GetComponent<Button>().interactable = false;
	}
	
	// Update is called once per frame
	void Update () {
        if (tutorialPiece.GetComplete())
        {
            GetComponent<Button>().interactable = true;
            Destroy(this);
        }
        
	}
}
