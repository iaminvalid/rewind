﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialMomentPiece : TutorialPiece
{
    public TutorialMomentPiece nextPiece;
	public GameObject momentButton;
	public bool firstMomentPiece;
	// Use this for initialization
    void Update()
    {
		if (firstMomentPiece && timeManager.curTime > startTime)
			TriggerTutorial ();
    }
    public override void TutorialComplete()
    {
		if (timeManager == null)
			return;

        if (timeManager.curTime >= startTime && !isComplete)
        {
            tutorialManager.CompleteTutorialPiece(indexPosition);
            isComplete = true;
            textHolder.SetActive(false);

			if (nextPiece != null) {
				nextPiece.TriggerTutorial ();
			} 
        }
    }


    public override void TriggerTutorial()
    {

        wasTriggered = true;
        textHolder.SetActive(true);

		//Specifically for the Moments Clock tutorial
		if (this.name == "Tut_Moments")
		{
			timeManager.scrubBar.value = 5.9f;
			momentButton.SetActive (true);
			timeManager.PauseUpdate ();
		}

		//timeManager.PauseUpdate();
    }


	public void EndMoment()
	{
		if (nextPiece != null) {
			nextPiece.EndMoment ();
		}
		TutorialComplete ();
        if (unpauseTime)
        {
            timeManager.StartUpdate ();
		    timeManager.timeSpeedSlider.value = 1;
        }
	}
}
