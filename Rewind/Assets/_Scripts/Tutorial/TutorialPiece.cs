﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialPiece : MonoBehaviour {


    public float startTime;
    public bool unpauseTime;
    public TutorialManager tutorialManager;
    public GameObject textHolder;
    protected int indexPosition;
    protected bool wasTriggered;
	protected bool isComplete = false;
    protected TimeManager timeManager;

	// Use this for initialization
	void Start () {
        timeManager = GameObject.FindGameObjectWithTag("TimeManager").GetComponent<TimeManager>();
        
    }

    // Update is called once per frame
    void Update () {

        if (timeManager.curTime >= startTime && !wasTriggered)
            TriggerTutorial();
        
    }

    public virtual void TutorialComplete()
	{
        if (timeManager == null)
            return;

        if (!isComplete)
		{
            if (unpauseTime)
            {
			 //   timeManager.StartUpdate();
			 //   timeManager.timeSpeedSlider.value = 1;
            }

			if (this.name == "Tut_TimeCyclePause")
			{
				timeManager.StartUpdate ();
				timeManager.curTime = 5.8f;
				//momentButton.SetActive (true);
				timeManager.PauseUpdate ();
			}

			tutorialManager.CompleteTutorialPiece (indexPosition);
			isComplete = true;
			textHolder.SetActive (false);



		}

    }
	public virtual void ForceEnd()
	{
		if (!isComplete)
		{
			isComplete = true;
			wasTriggered = true;
			textHolder.SetActive (false);

		}
	}
    public virtual void SetIndex(int index)
    {
        indexPosition = index;
    }

    public virtual void TriggerTutorial()
    {

        wasTriggered = true;
        textHolder.SetActive(true);
     //   timeManager.timeSpeedSlider.value = 0;
     //   timeManager.PauseUpdate();
        
    }

    public bool GetComplete()
    {
        return isComplete;
    }
    
    public bool GetTriggered()
    {
        return wasTriggered;
    }
}
