﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialPuzzleCheck : MonoBehaviour {
    public PuzzlePiece puzzlePiece;
    public TutorialMomentPiece tutorialPiece;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (puzzlePiece.isSelected)
        {
			if (tutorialPiece.isActiveAndEnabled)
			tutorialPiece.EndMoment();
			
            Destroy(this);
        }
	}
}
