﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkipToEnd : MonoBehaviour {
	public float resetDuration;
	private float timer;
	private bool resetting;
	public float endTime = 90;
	private TimeManager timeManager;
	private VignetteControl camVignette;
	// Use this for initialization
	void Start () {
		timeManager = GameObject.FindGameObjectWithTag("TimeManager").GetComponent<TimeManager>();
		resetting = false;
		camVignette = Camera.main.GetComponent<VignetteControl> ();
	}

	// Update is called once per frame
	void Update () {
		if (resetting)
		{
			timeManager.scrubBar.value = (timer / resetDuration) * endTime; // changed to adjustable end time to stop application crashing bug

			timer += Time.unscaledDeltaTime;

		}
	}

	void LateUpdate()
	{
		if (resetting)
		{
			if (timer > resetDuration)
			{
				resetting = false;
				timeManager.scrubBar.value = endTime;
				//timeManager.skippingToEnd = false;
				camVignette.EndEffect ();
			}
		}
	}

	public void RestartTime()
	{
        if (timeManager.curTime == 0)
            timer = 0;
        else
			timer = timeManager.curTime / endTime;
         
        resetting = true;

		StartCoroutine (StopThatVignettePlease ());
	}

	IEnumerator StopThatVignettePlease()
	{
		yield return new WaitForSecondsRealtime (0.2f);
		camVignette.EndEffect ();
	}
}
