﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForceSetCurMoment : MonoBehaviour {
	public PuzzleState puzzleState;
	public PuzzleStartButton puzzleStartButton; 
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	public void ForceSetCurPuzzle()
	{
		if (puzzleState.curPuzzle != null) {
			return;
		}
		puzzleState.curPuzzle = puzzleStartButton;
	}
}
