﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForceWait : MonoBehaviour {
    private bool hasBeenTriggered = false;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Space))
        {

            hasBeenTriggered = !hasBeenTriggered;
            GetComponent<WaitNode>().ItemChange(hasBeenTriggered);
        }
	}
}
