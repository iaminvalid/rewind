﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimateInanimateObjectAnimatorOfAnimating : MonoBehaviour {
	private TimeManager timeManager;
	private Animator actorAnimator;
	private float speed;
	// Use this for initialization
	void Start () {
		timeManager = GameObject.FindGameObjectWithTag ("TimeManager").GetComponent<TimeManager> ();
		actorAnimator = GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (timeManager.curTime == 0)
			actorAnimator.speed = 0;
		else
			actorAnimator.speed = 1;

		if (timeManager.reverseTime)
			speed = -1;
		else
			speed = 1;



		actorAnimator.SetFloat("Speed", speed);
	}
}
