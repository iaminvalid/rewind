﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : Setting {
	//backgroundMusic and backgroundMomentMusic are both set to play on Awake
    public AudioSource backgroundMusic ;
	public AudioSource backgroundMomentMusic;
    public AudioSource interactionSound;
	public AudioSource sfxTicking;

	public AudioClip bgMusicClip;
	public AudioClip bgMomentMusicClip;
	public AudioClip solutionSelect;
	public AudioClip magicItemPickup;
	public AudioClip momentEntry;
	public AudioClip momentExit;
	//public AudioClip timeCycle;

	float musicVol;
	float sfxVol;

    // Use this for initialization
    void Start () {


        

        updateSettings();    
		backgroundMusic.clip = bgMusicClip;
		backgroundMomentMusic.clip = bgMomentMusicClip;

		// Do not play here. Now managed by Cutscene Manager
		//backgroundMusic.Play (); 
		//backgroundMomentMusic.Play ();
    }

    public override void updateSettings()
    {
		// Get volume
		musicVol = PlayerPrefs.GetInt("Music") == 0 ? 0 : 0.4f;
		sfxVol = PlayerPrefs.GetInt("SFX") == 0 ? 0 : 0.4f;
		// Set volume
		backgroundMusic.volume = musicVol;
		interactionSound.volume = sfxVol; 
		sfxTicking.volume = sfxVol;
    }

	public void PlayMagicItemPickup()
	{
		interactionSound.clip = magicItemPickup;
		interactionSound.Play ();
	}

	public void PlaySolutionSelect()
	{
		interactionSound.clip = solutionSelect;
		interactionSound.Play ();
	}

	public void PlayMomentEntry() // Called from Moment UI Buttons
	{
		interactionSound.clip = momentEntry;
		interactionSound.Play ();
	}

	public void PlayMomentExit() // Called from Moment UI Buttons
	{
		interactionSound.clip = momentExit;
		interactionSound.Play ();
	}

	public void MomentMusic() // Called from PuzzleState
	{
	//	backgroundMomentMusic.volume = musicVol;
		backgroundMusic.volume = 0f;
		backgroundMomentMusic.volume = musicVol;
	}

	public void NormalMusic() // Called from PuzzleState
	{

	//	backgroundMusic.volume = musicVol;
		backgroundMusic.volume = musicVol;
		backgroundMomentMusic.volume = 0f;
	}
}
