﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainGameSettings : MonoBehaviour {
    public GameObject mainCamera;
    public GameObject UICanvas;
    public GameObject settingsCanvas;
    public Setting[] settingList;
    public SettingsStore settingsStore;

    public void OpenMenu()
    {
        mainCamera.GetComponent<InputScript>().enabled = false;
        UICanvas.SetActive(false);
        settingsCanvas.SetActive(true);
        //settingsStore.SetSettings();
    }

    public void CloseMenu()
    {
        mainCamera.GetComponent<InputScript>().enabled = true;
        UICanvas.SetActive(true);
        settingsStore.updateSettings();
        settingsCanvas.SetActive(false);

        for (int i = 0; i < settingList.Length; i++)
            settingList[i].updateSettings();
        



    }
}
