﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackGroundMusic : MonoBehaviour
{
    private AudioSource audioSource;
    private TimeManager timeManager;
    // Use this for initialization
    void Start()
    {
        timeManager = GameObject.FindGameObjectWithTag("TimeManager").GetComponent<TimeManager>();
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {

        if (Mathf.Abs(timeManager.timeSpeedSlider.value) <= .5f)
            audioSource.pitch = 0.25f;
        else if (Mathf.Abs(timeManager.timeSpeedSlider.value) <= .75f)
            audioSource.pitch = 0.5f;
        else if (Mathf.Abs(timeManager.timeSpeedSlider.value) <= 2f)
            audioSource.pitch = 1;
        else if (Mathf.Abs(timeManager.timeSpeedSlider.value) <= 4f)
            audioSource.pitch = 1.25f;
        else if (Mathf.Abs(timeManager.timeSpeedSlider.value) < 6f)
            audioSource.pitch = 1.5f;

        if (timeManager.reverseTime)
            audioSource.pitch *= -1;


    }
}
