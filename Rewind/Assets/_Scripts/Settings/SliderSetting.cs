﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SliderSetting : Setting {

	// Use this for initialization
	void Start () {
        updateSettings();

    }
    public override void updateSettings()
    {
        if (PlayerPrefs.GetInt("ThymeSlider") == 0)
            gameObject.SetActive(false);
        else
            gameObject.SetActive(true);


    }
    // Update is called once per frame
    void Update () {
		
	}
}
