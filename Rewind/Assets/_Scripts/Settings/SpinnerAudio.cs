﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpinnerAudio : MonoBehaviour {
    private AudioSource audioSource;
    private TimeManager timeManager;
	// Use this for initialization
	void Start () {
        timeManager = GameObject.FindGameObjectWithTag("TimeManager").GetComponent<TimeManager>();
        audioSource = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
        if (timeManager.reverseTime)
        {
            audioSource.pitch = -1 -timeManager.timeSpeedSlider.value ;
        }
        else
        {
         
                audioSource.pitch = timeManager.timeSpeedSlider.value;

            
        }
    }
}
