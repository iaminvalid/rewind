﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class SettingsStore : MonoBehaviour {

    public Toggle SFX;
    public Toggle Music;
    public Toggle ThymeSlider;
	// Use this for initialization
	void Start () {
        PlayerPrefs.DeleteAll();
        DefaultSettings();
        SetSettings();
    }

    // Update is called once per frame
    void Update () {
		
	}

    void DefaultSettings()
    {
        PlayerPrefs.SetInt("Music", 1);
        PlayerPrefs.SetInt("SFX", 1);
        PlayerPrefs.SetInt("ThymeSlider", 0);
    }

    public void SetSettings()
    {
        if (PlayerPrefs.GetInt("SFX") == 0)
            SFX.isOn = false;
        else
            SFX.isOn = true;

        if (PlayerPrefs.GetInt("Music") == 0)
            Music.isOn = false;
        else 
            Music.isOn = true;

        if (PlayerPrefs.GetInt("ThymeSlider") == 0)
            ThymeSlider.isOn = false;
        else
            ThymeSlider.isOn = true;




    }

    public void updateSettings()
    {
        if (SFX.isOn)
            PlayerPrefs.SetInt("SFX", 1);
        else
            PlayerPrefs.SetInt("SFX", 0);

        if (Music.isOn)
            PlayerPrefs.SetInt("Music", 1);
        else
            PlayerPrefs.SetInt("Music", 0);

        if (ThymeSlider.isOn)
            PlayerPrefs.SetInt("ThymeSlider", 1);
        else
            PlayerPrefs.SetInt("ThymeSlider", 0);



        SetSettings();
    }
}
