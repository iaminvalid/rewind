﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MomentWheelRotation : MonoBehaviour {

    private TimeManager timeManager;
    private float maxTime;
    private float[] timeStages = new float[3];

	// Use this for initialization
	void Start () {
        timeManager = GameObject.FindGameObjectWithTag("TimeManager").GetComponent<TimeManager>();
        maxTime = timeManager.maxTime;
        timeStages[0] = maxTime / 4;
        timeStages[1] = maxTime / 2;
        timeStages[2] = timeStages[0] + timeStages[1];
	}
	
	// Update is called once per frame
	void Update () {
        if (timeManager.curTime == 0)
            return;



        if (timeManager.curTime < timeStages[0])
            transform.right = Vector2.Lerp(Vector2.right, Vector2.up, timeManager.curTime / timeStages[0]);

        if (timeManager.curTime < timeStages[1] && timeManager.curTime > timeStages[0])
            transform.right = Vector2.Lerp(Vector2.up, -Vector2.right, (timeManager.curTime - timeStages[0]) / timeStages[0]);

        if (timeManager.curTime < timeStages[2] && timeManager.curTime > timeStages[1])
        transform.right = Vector2.Lerp(-Vector2.right, -Vector2.up, (timeManager.curTime - timeStages[1]) / timeStages[0]);

        if (timeManager.curTime > timeStages[2])
        transform.right = Vector2.Lerp(-Vector2.up, Vector2.right, (timeManager.curTime - timeStages[2]) / timeStages[0]);


    }
}
