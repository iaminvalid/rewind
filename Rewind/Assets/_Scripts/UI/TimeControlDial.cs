﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TimeControlDial : Draggable
{
    private TimeManager timeManager;
    private Vector3 prevPos;
    

	public float curSpeed;
    private bool forwardTime;
    private bool directionLock;
    private bool downLock;
    private bool finalLock;
    public enum Direction { up, right, down, left };
    private Direction direction = Direction.up;
    private Direction prevDir = Direction.up;

	public TutorialPiece tutPiece;


    public float speedMultiplier;
    // Use this for initialization
    void Start () {
        timeManager = GameObject.FindGameObjectWithTag("TimeManager").GetComponent<TimeManager>();
	}

    public override void OnBeginDrag(PointerEventData eventData)
    {
        
        prevPos = Input.mousePosition;
        Vector2 curDir = (Input.mousePosition - GetComponent<RectTransform>().position).normalized;
        GetComponent<DialRollBack>().EndRollBack();



    }
    public override void OnDrag(PointerEventData eventData)
    {
        Vector2 curDir = (Input.mousePosition - GetComponent<RectTransform>().position).normalized;

        DirectionCheck(curDir);
        Rotate(curDir);
        if (!directionLock && prevDir == Direction.up && (direction == Direction.right || direction == Direction.left) )
            firstLock();
        
        prevDir = direction;
        curSpeed *= speedMultiplier;
        if (!forwardTime)
            curSpeed = Mathf.Abs(curSpeed) * -1;
        timeManager.timeSpeedSlider.value = curSpeed;
    }
    public override void OnEndDrag(PointerEventData eventData)
    {
        GetComponent<DialRollBack>().StartRollBack(direction,forwardTime);

		//Tutorial
		if (!tutPiece.GetComplete ())
			tutPiece.TutorialComplete ();
    }
    public void DirectionCheck(Vector2 a_curDir)
    {
        
        if (Vector2.Angle(-Vector2.up, a_curDir) < 45)
        {
            direction = Direction.down;
            UnlockLast();

            downLock = true;
            if (forwardTime)
                curSpeed = 1 +(Vector2.Angle(Vector2.right, a_curDir) / 90);
            else           
                curSpeed = 1 +(Vector2.Angle(-Vector2.right, a_curDir) / 90);

        }



        if (!finalLock)
        {

            if (Vector2.Angle(Vector2.up, a_curDir) < 45)
            {
                if (a_curDir.x < 0)
                    forwardTime = false;
                else
                    forwardTime = true;
                curSpeed = (Vector2.Angle(Vector2.up, a_curDir) / 90);
                directionLock = false;
                downLock = false;
                direction = Direction.up;
            }
        }

        if (Vector2.Angle(Vector2.right, a_curDir) < 45)
        {


            if (directionLock)
            {
                if (!forwardTime && downLock)
                {
                    LastLock();
                    curSpeed = 2 + (Vector2.Angle(-Vector2.up, a_curDir) / 90);
                }
            else if(!finalLock) 
                curSpeed =  (Vector2.Angle(Vector2.up, a_curDir) / 90);

            }

            direction = Direction.right;


        }
        if (Vector2.Angle(-Vector2.right, a_curDir) < 45)
            {



            if (forwardTime && downLock)
            {
                LastLock();
                curSpeed = 2 + (Vector2.Angle(-Vector2.up, a_curDir) / 90);
            }
            else if(!finalLock)
                curSpeed = (Vector2.Angle(Vector2.up, a_curDir) / 90);
            


            direction = Direction.left;


        }

    }


        
    


    private void firstLock()
    {
        directionLock = true;
        if (direction == Direction.right)
            forwardTime = true;
        else
            forwardTime = false;
        
    }

    private void LastLock()
    {
        finalLock = true;
    }
    public void ForuceUnlock()
    {
        direction = Direction.up;
        UnlockLast();
        directionLock = false;
        downLock = false;


    }

    private void UnlockLast()
    {
        finalLock = false;
    }
    private void Rotate(Vector2 a_curDir)
    {
        if (finalLock)
        {
            if (Vector2.Angle(-Vector2.up, a_curDir) < 45)
                transform.right = Input.mousePosition - transform.position;
            if (!forwardTime)
            {
                if (Vector2.Angle(Vector2.right, a_curDir) < 45)
                    transform.right = Input.mousePosition - transform.position;
     
            }
            else
            {
                if (Vector2.Angle(-Vector2.right, a_curDir) < 45)
                    transform.right = Input.mousePosition - transform.position;
     
            }
        }
        else
            transform.right = Input.mousePosition - transform.position;



    }
}
