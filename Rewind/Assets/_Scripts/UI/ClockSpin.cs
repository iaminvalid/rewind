﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClockSpin : MonoBehaviour {

	public AnimationCurve rotationYAnim;
	public AnimationCurve rotationZAnim;
	public AnimationCurve hoverYAnim;
	public AnimationCurve scaleAnim;
	public bool scaleOnly;

	float t;
	float prevTime;
	void Start ()
	{
	}

	// Update is called once per frame

	void Update(){
		t += Time.unscaledDeltaTime;

		if (!scaleOnly)
		{
			transform.localPosition = Vector3.up * hoverYAnim.Evaluate (t);
			transform.Rotate (0, rotationYAnim.Evaluate (t), rotationZAnim.Evaluate (t));
			transform.localScale = Vector3.one * scaleAnim.Evaluate (t);
		} else
		{
			transform.localScale = Vector3.one * scaleAnim.Evaluate (t);
		}
	}
	void FixedUpdate () {
	}
}
