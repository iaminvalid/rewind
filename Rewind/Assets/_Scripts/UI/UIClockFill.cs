﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIClockFill : MonoBehaviour {

	TimeControlDial timeDial;
	public Image forwardPanel;
	public Image reversePanel;

	[Range (0,1)]
	public float buffer;

	// Use this for initialization
	void Start () {
		timeDial = GameObject.FindObjectOfType (typeof(TimeControlDial)) as TimeControlDial;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		//Debug.Log (timeDial.curSpeed);

		forwardPanel.fillAmount = Mathf.Clamp (timeDial.curSpeed / 3.4f, 0, buffer);
		reversePanel.fillAmount = Mathf.Clamp (-(timeDial.curSpeed / 3.4f), 0, buffer);

	}
}
