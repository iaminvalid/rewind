﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuWheelRotation : MonoBehaviour {
    
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void RotateRight(float dist)
    {
        transform.Rotate(-Vector3.forward, dist);
    }

    public void RotateLeft(float dist)
    {
        transform.Rotate(Vector3.forward, dist);

    }
    public void RotateDropOff(float speed)
    {
        transform.Rotate(Vector3.forward, speed);
    }


}
