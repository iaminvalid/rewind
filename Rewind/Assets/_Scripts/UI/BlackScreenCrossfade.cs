﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class BlackScreenCrossfade : MonoBehaviour {
    
    public enum screenEnum
    {
        startScreen,
        endScreen
    }
    public screenEnum screenType;
	Image blackScreen;
	float alpha;
    bool fade;

	// Use this for initialization
	void Start () {
		blackScreen = GetComponent<Image> ();
		alpha = blackScreen.color.a;
        StartCoroutine(FadeDelay());
	}

    IEnumerator FadeDelay()
    {
        yield return new WaitForSecondsRealtime(1);
        fade = true;
    }

    // Update is called once per frame
    void Update() {
        if (screenType == screenEnum.endScreen) {
            blackScreen.CrossFadeAlpha(1, 4, true);
            if (alpha >= 1)
                SceneManager.LoadScene("Credits"); }
        else if (screenType == screenEnum.startScreen && fade)
        {
            blackScreen.CrossFadeAlpha(0, 1, true);
        }
	}
}
