﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetActiveTime : MonoBehaviour {
    public float startTime;
    public float endTime;
    public GameObject child;
    public bool isRunning = true;
    private bool isRunningDefault;
    private TimeManager timeManager;
	// Use this for initialization
	void Start () {
        isRunningDefault = isRunning;
        timeManager = GameObject.FindGameObjectWithTag("TimeManager").GetComponent<TimeManager>();
	}
	
	// Update is called once per frame
	void Update () {

        if (!isRunning)
        {
            child.SetActive(false);
            return;
        }


        if (timeManager.curTime > startTime && timeManager.curTime < endTime)
            child.SetActive(true);
        else
            child.SetActive(false);
        
	}
    public void UpdateIsRunning(bool switchValue)
    {
        if (switchValue)
            isRunning = !isRunningDefault;
        else
            isRunning = isRunningDefault;
    }
}
