﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TimeController : Draggable{
    public TimeManager timeManager;
    public float timeScaleChange;
    public float rotateSpeed;
    private Vector3 prevPos;
    private float curSpeed;
    private bool SlowTime;
    private float slowTimeSpeed;
    public AudioClip forwardTimeSound;
    public AudioClip backwardTimeSound;
    public AudioSource audioSource;
    void Update()    
    {
        if (SlowTime)
            TimeSlow();
        
    }
    public override void OnBeginDrag(PointerEventData eventData)
    {
        SlowTime = false;
        prevPos = Input.mousePosition;
    }
    public override void OnDrag(PointerEventData eventData)
    {
 //       audioSource.Play();
        Vector2 curDir = (Input.mousePosition - GetComponent<RectTransform>().position).normalized;
        Vector3 preDir = (prevPos - GetComponent<RectTransform>().position).normalized;

        

        if (Input.mousePosition.x < GetComponent<RectTransform>().position.x && Input.mousePosition.y > GetComponent<RectTransform>().position.y)
        {
            if (Vector2.Angle(Vector3.up, preDir) < Vector2.Angle(Vector3.up, curDir))
                RotateLeft();
            else
                RotateRight();
            
        }
        else if (Input.mousePosition.x > GetComponent<RectTransform>().position.x && Input.mousePosition.y > GetComponent<RectTransform>().position.y)
        {
            if (Vector2.Angle(Vector3.right, preDir) < Vector2.Angle(Vector3.right, curDir))
                RotateLeft();
            else
                RotateRight();
        }
        else if (Input.mousePosition.x < GetComponent<RectTransform>().position.x && Input.mousePosition.y < GetComponent<RectTransform>().position.y)
        {
            if (Vector2.Angle(-Vector3.right, preDir) < Vector2.Angle(-Vector3.right, curDir))
                RotateLeft();
            else
                RotateRight();
        }
        else if (Input.mousePosition.x > GetComponent<RectTransform>().position.x && Input.mousePosition.y < GetComponent<RectTransform>().position.y)
        {
            if (Vector2.Angle(-Vector3.up, preDir) < Vector2.Angle(-Vector3.up, curDir))
                RotateLeft();
            else
                RotateRight();
        }




        prevPos = Input.mousePosition;
        transform.right = Input.mousePosition - transform.position; 
 //       audioSource.Pause();
    }
    public override void OnEndDrag(PointerEventData eventData)
    {
        SlowTime = true;
        slowTimeSpeed =Mathf.Abs( curSpeed);
        base.OnEndDrag(eventData);
    }
    private void RotateRight()
    {

//        if (audioSource.clip != forwardTimeSound)
//            audioSource.clip = forwardTimeSound;
//
//        audioSource.Play();
        
        //timeManager.slider.value += timeScaleChange * Time.unscaledDeltaTime;
        timeManager.timeSpeedSlider.value = Mathf.Abs( Vector2.Distance(prevPos, Input.mousePosition) * Time.unscaledDeltaTime);
        curSpeed = timeManager.timeSpeedSlider.value;
    }
    private void RotateLeft()
    {
        //timeManager.slider.value -= timeScaleChange * Time.unscaledDeltaTime;
        timeManager.timeSpeedSlider.value = 2 * -Mathf.Abs(Vector2.Distance(prevPos, Input.mousePosition) * Time.unscaledDeltaTime);
        curSpeed = timeManager.timeSpeedSlider.value;
    }

    private void TimeSlow()
    {
        if (curSpeed <0)
        {
            curSpeed += Time.unscaledDeltaTime * slowTimeSpeed ;
            if (curSpeed > 0)
            {
                SlowTime = false;
            curSpeed = 0;
            }
        }
        else if (curSpeed > 0)
        {
            curSpeed -= Time.unscaledDeltaTime * slowTimeSpeed;
            if (curSpeed < 0)
            {
                SlowTime = false;
                curSpeed = 0;
            }
        }
            timeManager.timeSpeedSlider.value = curSpeed;

   }

}
