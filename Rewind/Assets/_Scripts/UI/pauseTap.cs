﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class pauseTap : MonoBehaviour {
	public TutorialPiece tutPiece;
    private TimeManager timeManager;
    public float clickDuration;
    private float timer;
    private bool isClicked;
	// Use this for initialization
	void Start () {
        timeManager = GameObject.FindGameObjectWithTag("TimeManager").GetComponent<TimeManager>();
        timer = clickDuration;
	}
	
	// Update is called once per frame
	void Update () {

        if (isClicked)
        {
            timer -= Time.unscaledDeltaTime;
            if (Input.GetMouseButtonUp(0))
                Release();
        }
	}

    public void FirstClick()
    {
        if (isClicked)
            return;
		
        isClicked = true;
        
    }
    void Release()
    {
		if (timer > 0) {
			timeManager.timeSpeedSlider.value = timeManager.timeSpeedSlider.value == 0 ? 1 : 0;
			tutPiece.TutorialComplete ();
		}

        timer = clickDuration;
        isClicked = false;
    }
}
