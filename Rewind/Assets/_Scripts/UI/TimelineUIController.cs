﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimelineUIController : MonoBehaviour {

	public RectTransform timelineCursor;
	public RectTransform timelineBar;
	public Slider timelineSlider;
	public float buffer;
	Vector2 minPos;
	Vector2 maxPos;

	bool sliderActive;
	float percent;

	TimeManager timeManager;

	// Use this for initialization
	void Start () {
		timeManager = GameObject.Find ("TimeManager").GetComponent<TimeManager> ();
		minPos = new Vector2(timelineBar.rect.xMin + buffer, timelineCursor.localPosition.y);
		maxPos = new Vector2(timelineBar.rect.xMax - buffer, timelineCursor.localPosition.y);

		// Set starting pos of the cursos
		timelineCursor.localPosition = minPos ;



	}


	// Update is called once per frame
	void Update () {
		// Moves from timelinemin to timelinemax x pos by the percentage of current time to max time
		//percent = Vector2.Lerp (minPos, maxPos, timeManager.curTime / timeManager.maxTime);

		percent = timelineSlider.value / timelineSlider.maxValue;
		timelineCursor.localPosition = Vector2.Lerp(minPos, maxPos, percent);
	}

		public void SliderActive (bool b) // Called from Scrub Bar Slider's Event Data (Inspector)
	{
		sliderActive = b;
	}



}
