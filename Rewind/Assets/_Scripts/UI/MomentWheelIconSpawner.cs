﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MomentWheelIconSpawner : MonoBehaviour {

    public GameObject[] iconList;
	Transform timeline;
	Transform timelineCursor;
    public float distUpWhenSpawning;

   
	// Use this for initialization
	void Start () {
		timeline = GameObject.Find ("TimelineGroup").transform;
		timelineCursor = GameObject.Find ("Image_TimelineCursor").transform;
	}
	
	// Update is called once per frame
	void Update () {

	}
    public void AddIcon(int index)
    {
		GameObject newIcon = Instantiate(iconList[index], timelineCursor);
		newIcon.transform.SetParent (timeline);
		newIcon.transform.localScale = Vector3.one * 0.8f;
//        newIcon.transform.localPosition = new Vector3(0, 0, 0);
//        newIcon.transform.position = newIcon.transform.position + new Vector3(0,distUpWhenSpawning,0);
//		newIcon.transform.up = new Vector3 (0, 1, 0);
	}
}
