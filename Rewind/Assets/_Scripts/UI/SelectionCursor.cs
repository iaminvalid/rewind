﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectionCursor : MonoBehaviour {
    public GameObject[] buttonList;
    private Vector2 size;
	// Use this for initialization
	void Start () {
        size = new Vector2(Screen.width, Screen.height);
        for (int i = 0; i < buttonList.Length; i++)
        {
            buttonList[i].GetComponent<RectTransform>().rect.Set(buttonList[i].GetComponent<RectTransform>().position.x + Screen.width, buttonList[i].GetComponent<RectTransform>().position.y + Screen.height, buttonList[i].GetComponent<RectTransform>().rect.width, buttonList[i].GetComponent<RectTransform>().rect.height);
        }
	}

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonUp(0))
        {

            for (int i = 0; i < buttonList.Length; i++)
            {
                buttonList[i].GetComponent<MenuSelectionButton>().NextScene();
            }


        }
        //GetComponent<RectTransform>().rect.Set(transform.position.x, transform.position.y, 60, 60);
        GetComponent<RectTransform>().localScale = new Vector2(( Screen.width / size.x) , (Screen.height / size.y ) );

        for (int i = 0; i < buttonList.Length; i++)
        {

            if (Intersect(GetComponent<RectTransform>(), buttonList[i].GetComponent<RectTransform>()))
            {
                buttonList[i].GetComponent<MenuSelectionButton>().IsSelected(true);
            }
            else
                buttonList[i].GetComponent<MenuSelectionButton>().IsSelected(false);

        }
    }
     public static bool Intersect(RectTransform a, RectTransform b)
    {
        Rect aRect = a.rect;
        Rect bRect = b.rect;

        float aWidth =  (a.rect.width/2 );
        float aHeight = (a.rect.height/2);
        float bWidth =  (b.rect.width/2 );
        float bHeight = (b.rect.height/2);

        bool c1 = a.position.x - aWidth < b.position.x + bWidth;
        bool c2 = a.position.x + aWidth > b.position.x - bWidth;
        bool c3 = a.position.y - aHeight< b.position.y + bWidth;
        bool c4 = a.position.y + aHeight> b.position.y - bWidth;

        return (c1 && c2 && c3 && c4);



    }

    

}
