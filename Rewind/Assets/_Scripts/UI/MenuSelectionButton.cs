﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class MenuSelectionButton : SceneTransition {
    public bool isSelected;
    public Sprite selectImage;
    public Sprite deselectImage;

    private SettingsStore settings;
    // Use this for initialization
	void Start () {
        settings = GameObject.FindGameObjectWithTag("Settings").GetComponent<SettingsStore>();
	}
	
	// Update is called once per frame
	void Update () {

        

    }
    public void IsSelected(bool value)
    {
        if (value)
        {
            isSelected = true;
            GetComponent<Image>().sprite = selectImage;
        }
        else
        {
            
            isSelected = false;
            GetComponent<Image>().sprite = deselectImage;
        }
    }
    public override void NextScene()
    {
        if (isSelected)
        {
            settings.updateSettings();
            base.NextScene();
        }
    }
}
