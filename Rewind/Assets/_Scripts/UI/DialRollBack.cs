﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialRollBack : MonoBehaviour {
    public float rollBackTime;
    private TimeControlDial controlDial;
    private TimeManager timeManager;
    private bool forwardTime;
    private bool rollBack;
    private float degreeMin;
    private float degreeMax;
	// Use this for initialization
	void Start () {
        timeManager = GameObject.FindGameObjectWithTag("TimeManager").GetComponent<TimeManager>();
        controlDial = GetComponent<TimeControlDial>();
        degreeMin = 90 - (rollBackTime * Time.unscaledDeltaTime * 200);
        degreeMax = 90 + (rollBackTime * Time.unscaledDeltaTime * 200);
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        if (!rollBack)
            return;
		//If going in reverse
		if (!forwardTime && controlDial.curSpeed < -0.2f)
        {
            //if PC build is funky delete the * 60
            transform.Rotate(Vector3.forward * -rollBackTime * 60 * Time.unscaledDeltaTime);
            controlDial.curSpeed += (rollBackTime - 1.5f) * Time.unscaledDeltaTime;
        }
		// if going forward
		else if (forwardTime && controlDial.curSpeed > 0.2f)
        {
            transform.Rotate(Vector3.forward * rollBackTime * 60 * Time.unscaledDeltaTime);
            controlDial.curSpeed -= (rollBackTime - 1.5f) *  Time.unscaledDeltaTime;

        }
		// Check to see if  at 0
		if (transform.localEulerAngles.z > degreeMin && transform.localEulerAngles.z < degreeMax)
		{
			
			controlDial.curSpeed = 0;
			controlDial.ForuceUnlock();
			rollBack = false;
			
			transform.right = Vector3.up;
			
		}
        if (Mathf.Abs(controlDial.curSpeed) < 0.1f)
            controlDial.curSpeed = 0;
        
           
            timeManager.timeSpeedSlider.value = controlDial.curSpeed;

        

    }

    public void StartRollBack(TimeControlDial.Direction a_direction, bool a_forwardTime)
    {
        rollBack = true;
        forwardTime = a_forwardTime;

    }

    public void EndRollBack()
    {
        rollBack = false;
        controlDial.curSpeed = 0;
    }




}
