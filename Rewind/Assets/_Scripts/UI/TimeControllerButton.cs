﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TimeControllerButton : Draggable {
    public TimeController timeController;
	public TutorialPiece tutorialPiece;

	bool tutComplete;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public override void OnBeginDrag(PointerEventData eventData)
    {
        timeController.OnBeginDrag(eventData);
    }

    public override void OnDrag(PointerEventData eventData)
    {
        timeController.OnDrag(eventData);
    }
    public override void OnEndDrag(PointerEventData eventData)
    {
        timeController.OnEndDrag(eventData);
		if (!tutComplete)
		{
			tutorialPiece.TutorialComplete ();
			tutComplete = true;
		}
    }

}
