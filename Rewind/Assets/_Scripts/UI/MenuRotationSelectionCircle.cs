﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MenuRotationSelectionCircle : Draggable 
{
    public MenuWheelRotation menuWheel;
    public float rotationSpeedMod;
    private bool isSelected;
    private Vector3 prevPos;
    private float curSpeed;
    private bool SlowTime;
    private float slowTimeSpeed;

    
    void Update()
    {
        if (SlowTime)
            TimeSlow();

        if (isSelected)
        {

            Vector2 curDir = (Input.mousePosition - GetComponent<RectTransform>().position).normalized;
            Vector3 preDir = (prevPos - GetComponent<RectTransform>().position).normalized;
            float dist = Vector2.Distance(curDir, preDir);


            if (Input.mousePosition.x < GetComponent<RectTransform>().position.x && Input.mousePosition.y > GetComponent<RectTransform>().position.y)
            {


                if (Vector2.Angle(Vector3.up, preDir) < Vector2.Angle(Vector3.up, curDir))
                    RotateLeft(dist);
                else
                    RotateRight(dist);



            }
            else if (Input.mousePosition.x > GetComponent<RectTransform>().position.x && Input.mousePosition.y > GetComponent<RectTransform>().position.y)
            {
                if (Vector2.Angle(Vector3.right, preDir) < Vector2.Angle(Vector3.right, curDir))
                    RotateLeft(dist);
                else
                    RotateRight(dist);
            }
            else if (Input.mousePosition.x < GetComponent<RectTransform>().position.x && Input.mousePosition.y < GetComponent<RectTransform>().position.y)
            {
                if (Vector2.Angle(-Vector3.right, preDir) < Vector2.Angle(-Vector3.right, curDir))
                    RotateLeft(dist);
                else
                    RotateRight(dist);
            }
            else if (Input.mousePosition.x > GetComponent<RectTransform>().position.x && Input.mousePosition.y < GetComponent<RectTransform>().position.y)
            {
                if (Vector2.Angle(-Vector3.up, preDir) < Vector2.Angle(-Vector3.up, curDir))
                    RotateLeft(dist);
                else
                    RotateRight(dist);
            }




            prevPos = Input.mousePosition;
            transform.right = Input.mousePosition - transform.position;


            if (Input.GetMouseButtonUp(0))
                DeSelect();
        }
    }

    private void RotateRight(float dist)
    {

       
        menuWheel.RotateRight(dist * rotationSpeedMod);
    }
    private void RotateLeft(float dist)
    {
       
        menuWheel.RotateLeft(dist * rotationSpeedMod);
    }

    private void TimeSlow()
    {
        if (curSpeed < 0)
        {
            curSpeed += Time.unscaledDeltaTime * slowTimeSpeed;
            if (curSpeed > 0)
            {
                SlowTime = false;
                curSpeed = 0;
            }
        }
        else if (curSpeed > 0)
        {
            curSpeed -= Time.unscaledDeltaTime * slowTimeSpeed;
            if (curSpeed < 0)
            {
                SlowTime = false;
                curSpeed = 0;
            }
        }

        menuWheel.RotateDropOff(curSpeed);
    }

    public void IsSelected()
    {
        SlowTime = false;
        prevPos = Input.mousePosition;
        isSelected = true;
    }
    private void DeSelect()
    {
        SlowTime = true;
        slowTimeSpeed = Mathf.Abs(curSpeed);
        isSelected = false;
    }

}
