﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseReturnButton : MonoBehaviour {
    private TimeManager timeManager;
	public GameObject childButton;
    // Use this for initialization
	void Start () {
        timeManager = GameObject.FindGameObjectWithTag("TimeManager").GetComponent<TimeManager>();
	}
	
	// Update is called once per frame
	void Update () {
        if (timeManager.timeSpeedSlider.value == 0)
            childButton.SetActive(true);
        else
            childButton.SetActive(false);
        
	}
}
