﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public class SelectionCircle : MonoBehaviour {
    private bool isSelected;
	// Use this for initialization
	void Start () {

        
	}
	
	// Update is called once per frame
	void Update () {

        if (isSelected)
            GetComponent<RectTransform>().right = Input.mousePosition - transform.position;
        


        if (Input.GetMouseButtonUp(0))
            DeSelect();
	}


    public void IsSelected()
    {
        isSelected = true;
    }
    private void DeSelect()
    {
        isSelected = false;
    }
}
