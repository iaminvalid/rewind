﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultiNodeReciever : MultiNode {
    public MultiNode sendingNode;
    private float secondPreviousArriveTime;

    // Use this for initialization
    
    protected override void AdditionalStart()
    {

        elementZero = sendingNode.elementZero;
        
        firstNodeDefault = elementZero;

        previousArriveTime = possibleNodes[0].GetComponent<Node>().GetArriveTime();
        secondPreviousArriveTime = possibleNodes[1].GetComponent<Node>().GetArriveTime();

        UpdateNode();

    }
    
    
    public override void UpdateNode()
    {
        if (elementZero)
            previousNode = possibleNodes[0];
        else
            previousNode = possibleNodes[1];

 
    }
    public override void ItemChange(bool trigger)
    {
        if (trigger)
            elementZero = !firstNodeDefault;
        else
            elementZero = firstNodeDefault;

        UpdateNode();
    }
    public override Vector3 Move(float curTime, Vector3 position)
    {
        if (elementZero)
            return base.Move(curTime, position);
    
        if (!isFirstNode)
            return Vector3.Lerp(previousNode.transform.position, transform.position, Mathf.Abs(curTime - secondPreviousArriveTime) / (arriveTime - secondPreviousArriveTime));
        else if (isFirstNode)
        {
            if (curTime == 0)
                return position;
    
            return Vector3.Lerp(charStartPos, transform.position, curTime / arriveTime);
    
        }
        else if (isEndNode)
        {
            //check state of game and see if she is safe;
        }
    
        return position;
    
    }

    public override GameObject transitionNode(float curTime)
    {
        if ((canMove || curTime < arriveTime) && (curTime > exitWaitTime || curTime < arriveTime))
        {
            if (elementZero)
                return base.transitionNode(curTime);
            else
            {
                if (!isEndNode)
                {

                    if (curTime > GetComponent<Node>().arriveTime)
                    {
                        return GetNextNode();
                    }
                }
                if (!isFirstNode && curTime < secondPreviousArriveTime)
                    return GetPreviousNode();
            }
        }


        return gameObject;



    }
}
