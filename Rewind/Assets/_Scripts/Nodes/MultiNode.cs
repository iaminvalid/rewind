﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultiNode : WaitNode {

    public GameObject[] possibleNodes = new GameObject[2];
    public bool elementZero;
    protected bool firstNodeDefault;

    // Use this for initialization


    protected override void AdditionalStart()
    {
        base.AdditionalStart();
        firstNodeDefault = elementZero;

        possibleNodes[0].GetComponent<Node>().previousNode = gameObject;
        possibleNodes[1].GetComponent<Node>().previousNode = gameObject;


        UpdateNode();
        if (!isFirstNode)
            previousArriveTime = previousNode.GetComponent<Node>().GetArriveTime();


    }



    // Update is called once per frame
    void Update () {
		
	}

    public void SetDirection(bool a_firstNode)
    {
        elementZero = a_firstNode;
        UpdateNode();
        
    }
    virtual public void UpdateNode()
    {

        if (elementZero)
            NextNode = possibleNodes[0];
        else
            NextNode = possibleNodes[1];
    }
    public override void UpdateOtherNodes(float duration)
    {
        this.arriveTime += duration;
        if (!isEndNode)
        {
            possibleNodes[0].GetComponent<Node>().UpdateOtherNodes(duration);
            possibleNodes[1].GetComponent<Node>().UpdateOtherNodes(duration);
        }
    }
    public override void ItemChange(bool trigger)
    {
        if (trigger)
            elementZero = !firstNodeDefault;
        else             
            elementZero =  firstNodeDefault;

        UpdateNode();
    }

    
}
