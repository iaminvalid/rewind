﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node : MonoBehaviour {
    public bool isFirstNode = false;
    public bool isEndNode = false;
    public float arriveTime;
    public string stateName;
    public GameObject previousNode;
    public GameObject NextNode;


    protected TimeManager timeManager;
    protected float previousArriveTime;


    protected Vector3 charStartPos;
    

	// Use this for initialization
	void Awake () {


        

        timeManager = GameObject.FindGameObjectWithTag("TimeManager").GetComponent<TimeManager>();
        if (!isEndNode)
            NextNode.GetComponent<Node>().previousNode = gameObject;
        
        
        
    }
    private void Start()
    {
        if (!isFirstNode)
        {
            transform.LookAt(previousNode.transform);
            transform.forward = -transform.forward;

        }
        AdditionalStart();

    }
    virtual public Vector3 GetDirection()
    {
            return transform.forward;
        
    }
    // Update is called once per frame
    void Update () {
		
	}
    virtual public float GetArriveTime()
    {
        return arriveTime;
    }
    virtual protected void AdditionalStart()
    {
        
        if (!isFirstNode)
            previousArriveTime = previousNode.GetComponent<Node>().GetArriveTime();
    }

    public GameObject GetNextNode()
    {
        
        return NextNode;
    }
    public virtual string GetAnimationState()
    {
        return stateName;
    }
    public GameObject GetPreviousNode()
    {
        return previousNode;
    }
	public float GetPreviousArriveTime()
	{
		return previousArriveTime;
	}
    virtual public Vector3 Move(float curTime, Vector3 position)
    {

        if (!isFirstNode)
			return Vector3.Lerp(previousNode.transform.position, transform.position, Mathf.Abs(curTime - previousArriveTime) / (arriveTime - previousArriveTime));
        else if (isFirstNode)
        {
            if (curTime == 0)
                return transform.position;
            
            return Vector3.Lerp(charStartPos, transform.position, curTime/ arriveTime);

        }
        else if (isEndNode)
        {
            //check state of game and see if she is safe;
        }

        return position;
        
    }
    virtual public GameObject transitionNode(float curTime)
    {
        if (!isEndNode)
        {

            if (curTime > GetComponent<Node>().arriveTime)
            {
                return GetNextNode();
            }
        }
        if (!isFirstNode && curTime < previousArriveTime)
            return GetPreviousNode();

        return gameObject;
    }
    public void NewNode(GameObject newNode,float duration, float curTime)
    {
        newNode.GetComponent<WaitNode>().previousNode = gameObject;
        newNode.GetComponent<WaitNode>().NextNode = NextNode;
        NextNode.GetComponent<Node>().previousNode = newNode;
        NextNode = newNode;

        newNode.GetComponent<WaitNode>().arriveTime = this.arriveTime;
        this.arriveTime = this.arriveTime - curTime;
        
       
    }
    public virtual void UpdateOtherNodes(float duration)
    {
        this.arriveTime += duration;
        if (!isEndNode)
            NextNode.GetComponent<Node>().UpdateOtherNodes(duration);
    }
    public virtual void ItemChange(bool trigger)
    {

    }
    public void SetStartPos(Vector3 startPos)
    {
        charStartPos = startPos;
    }


}
