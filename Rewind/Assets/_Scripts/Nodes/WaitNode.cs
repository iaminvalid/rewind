﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaitNode : Node
{
    public string waitStateName;
    public bool canMove;
    public float exitWaitTime;

    private bool canMoveDefault;
    private Vector3 origionalForward;

    protected override void AdditionalStart()
    {
        origionalForward = transform.forward;
        canMoveDefault = canMove;
        if (!isFirstNode)
            previousArriveTime = previousNode.GetComponent<Node>().GetArriveTime();
        
    }
    public override Vector3 Move(float curTime, Vector3 position)
    {

        if ((Mathf.Abs(curTime - previousArriveTime) / arriveTime - previousArriveTime) > 1 )
            return transform.position;

        if (!isFirstNode)
            return Vector3.Lerp(previousNode.transform.position, transform.position, Mathf.Abs(curTime - previousArriveTime) / (arriveTime - previousArriveTime));
        else if (isFirstNode)
        {
            if (curTime == 0)
                return position;

            return Vector3.Lerp(charStartPos, transform.position, curTime / arriveTime);

        }
        else if (isEndNode)
        {
            //check state of game and see if she is safe;
        }

        return transform.position;
    }
    public override GameObject transitionNode(float curTime)
    {
        if ((canMove  || curTime < arriveTime) && (curTime > exitWaitTime || curTime < arriveTime))
            return base.transitionNode(curTime);
        
        return gameObject;

        

    }
    public override string GetAnimationState()
    {
        if (timeManager.curTime > arriveTime)
            return waitStateName;
        else
            return stateName;
    }

    private int CheckWaitTimer(float curtime)
    {
        return 0;
    }
    public void StartWait()
    {

    }
    public override void ItemChange(bool trigger)
    {
        if (trigger)
            canMove = !canMoveDefault;
        else
            canMove = canMoveDefault;

    }

    public override float GetArriveTime()
    {
        return exitWaitTime < arriveTime ? arriveTime : exitWaitTime;
    }
    //public override Vector3 GetDirection()
    //{
    //    if ((Mathf.Abs(timeManager.curTime - previousArriveTime) / arriveTime - previousArriveTime) > 1)
    //        transform.forward = origionalForward;
    //    else
    //    {
    //        transform.LookAt(NextNode.transform);
    //        transform.forward = -transform.forward;
    //    }
    //
    //    return base.GetDirection();
    //}
}
