﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
public class InputScript : MonoBehaviour {

    public GameObject SelectedMagicItem;
    public WorldMovement streetCube;

    public Slider zoomSlider;

    public float zoomMin;
    public float zoomMax;

    private float persZoomMin;
    private float persZoomMax;

    public float sensitivity;
	public float orthoSensitivity;

    public TutorialPiece tutorialPiece;
    private Vector3 curPos;
    private Vector3 startpos;

	//For Tutorial
	bool tutComplete;

	// Use this for initialization
	void Start () {

        persZoomMin = GetComponent<Camera>().fieldOfView - 40;
        persZoomMax = persZoomMin + 50;
        startpos = transform.position;
	}

    // Update is called once per frame
    void LateUpdate()
    {

        Ray ray = GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

        if (Input.GetMouseButtonDown(0) && Input.touchCount < 2)

        {

            // if left button pressed...
            if (Physics.Raycast(ray, out hit))  
            {
                if (hit.transform.tag == "ClockTarget")
                {

                    gameObject.GetComponent<PuzzleState>().curPuzzle = hit.transform.GetComponentInParent<PuzzleStartButton>();
                    gameObject.GetComponent<PuzzleState>().StartPuzzle();
                    hit.transform.parent.gameObject.GetComponent<PuzzleStartButton>().isclicked = true;

                }
                else if (hit.transform.tag == "Draggable")
                    hit.transform.GetComponent<MouseDrag>().StartClick();
                else if (hit.transform.tag == "Tappable")
                {
                    if (!hit.transform.gameObject.GetComponent<MagicItem>())
                        hit.transform.GetComponent<PuzzlePiece>().Tapped();
                    
                    else
                        if(hit.transform.GetComponent<MagicItem>() != null)
                            hit.transform.GetComponent<MagicItem>().PickUp();
                }


            }


            
            
			if (EventSystem.current.currentSelectedGameObject == null && (hit.transform == null || hit.transform.tag == "Untagged"))
                streetCube.StartClick();
            
            if (EventSystem.current.currentSelectedGameObject != null)
                if (EventSystem.current.currentSelectedGameObject.GetComponent<pauseTap>() != null)
                EventSystem.current.currentSelectedGameObject.GetComponent<pauseTap>().FirstClick();

            return;
        }
        if (Input.GetMouseButtonUp(0))
        {
            if (Physics.Raycast(ray, out hit))
            {
                
                if (hit.transform.GetComponent<MagicItemPuzzlePiece>() != null)
                {

                    hit.transform.GetComponent<MagicItemPuzzlePiece>().CheckCurrentItem(SelectedMagicItem);

                    SelectedMagicItem = null;
                    return;
                }
            }
            SelectedMagicItem = null;
        }
        


        //if (Input.GetKeyDown(KeyCode.LeftArrow))
        //{
        //    GetComponent<Camera>().orthographicSize -= 10 * Time.unscaledDeltaTime;
        //
        //    if (GetComponent<Camera>().orthographicSize < 
        //    {
        //
        //    }
        //}


        ///'zoom'
        if (Input.touchCount >= 2)
        {
            //temp.GetComponentInChildren<Text>().text = "two touch";
			streetCube.EndClick();
            curPos = transform.position;
        
            Touch tZero = Input.GetTouch(0);
            Touch tOne = Input.GetTouch(1);
        
            Vector2 tZeroPrevPos = tZero.position - tZero.deltaPosition;
            Vector2 tOnePrevPos = tOne.position - tOne.deltaPosition;
        
        
            float prevTouchDeltaMag = (tZeroPrevPos - tOnePrevPos).magnitude;
            float touchDeltaMag = (tZero.position - tOne.position).magnitude;
        
            float deltaMagDiff = prevTouchDeltaMag - touchDeltaMag;
        
        
            GetComponent<Camera>().orthographicSize += (deltaMagDiff) * Time.unscaledDeltaTime;

                GetComponent<Camera>().fieldOfView = Mathf.Clamp(GetComponent<Camera>().fieldOfView + (deltaMagDiff) * Time.unscaledDeltaTime , persZoomMin, persZoomMax);

            if (GetComponent<Camera>().orthographicSize > zoomMax || GetComponent<Camera>().orthographicSize < zoomMin)
                GetComponent<Camera>().orthographicSize = GetComponent<Camera>().orthographicSize > zoomMax ? zoomMax : zoomMin;


            
        
        }
        if (Input.GetAxis("Mouse ScrollWheel") != 0)
        {
            if (GetComponent<Camera>().orthographic)
            {
                GetComponent<Camera>().orthographicSize -= Input.GetAxis("Mouse ScrollWheel") * orthoSensitivity * Time.unscaledDeltaTime;


                if (GetComponent<Camera>().orthographicSize > zoomMax || GetComponent<Camera>().orthographicSize < zoomMin)
                    GetComponent<Camera>().orthographicSize = GetComponent<Camera>().orthographicSize > zoomMax ? zoomMax : zoomMin;
            }
            else
            {
                GetComponent<Camera>().fieldOfView = Mathf.Clamp(GetComponent<Camera>().fieldOfView - Input.GetAxis("Mouse ScrollWheel") * sensitivity, persZoomMin, persZoomMax);
				if (!tutComplete && tutorialPiece.gameObject.activeSelf)
				{
                    tutorialPiece.TutorialComplete ();
					tutComplete = true;
				}
            }
        }
    }
}
