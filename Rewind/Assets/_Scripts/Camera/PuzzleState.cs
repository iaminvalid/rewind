﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class PuzzleState : MonoBehaviour {
    private TimeManager timeManager;

    public float timeScale;
    public GameObject returnButton;
    public GameObject revertButton;
    public GameObject[] returnClocks;
    public GameObject tintObject;
    public GameObject timeController;
	public GameObject timeLine;
    public GameObject momentWheel;
    public GameObject momentButtonHolder;
	public GameObject momentExitHolder;
    public PuzzleStartButton curPuzzle;
    public PuzzlePiece[] puzzleArray;
	public AudioManager audioSettings;

    private VignetteControl vignette;

    void Start()
    {
        vignette = GetComponent<VignetteControl>();
    timeManager = GameObject.FindGameObjectWithTag("TimeManager").GetComponent<TimeManager>();
		audioSettings = AudioManager.FindObjectOfType (typeof(AudioManager)) as AudioManager;

    }
    // Use this for initialization
    public void StartPuzzle()
    {
        curPuzzle.StartPuzzle();
        vignette.StartEffect();
		audioSettings.MomentMusic ();

        for (int i = 0; i < puzzleArray.Length; i++)
            puzzleArray[i].TurnOnParticle();
        
	
	momentExitHolder.SetActive (true);
        revertButton.SetActive(false);
	momentButtonHolder.SetActive(false);

        momentWheel.SetActive(false);
        timeController.SetActive(false);
		timeLine.SetActive (false);
        returnButton.SetActive(true);
        timeManager.timeSpeedSlider.value =  timeManager.AdjustFloat( timeScale);
        tintObject.SetActive(true);

    }
    public void EndPuzzle()
    {

        revertButton.SetActive(true);
        for (int i = 0; i < returnClocks.Length; i++)
            returnClocks[i].SetActive(false);
        

		momentExitHolder.SetActive (false);
        curPuzzle.EndPuzzle();
        vignette.EndEffect();
		audioSettings.NormalMusic ();

        for (int i = 0; i < puzzleArray.Length; i++)
            puzzleArray[i].TurnOffParticle();

        momentButtonHolder.SetActive(true);

        momentWheel.SetActive(true);
        curPuzzle.isclicked = false;
        tintObject.SetActive(false);
        returnButton.SetActive(false);
        timeController.SetActive(true);
		timeLine.SetActive (true);
        timeManager.timeSpeedSlider.value = 1;
        curPuzzle = null;

    }


}
