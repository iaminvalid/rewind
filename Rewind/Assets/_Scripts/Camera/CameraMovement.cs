﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour {

    public GameObject OtherCamera;
    private Vector3 startPosition;
    private Quaternion startRotation;

    public float startTimer;
    
    public bool move;
    public bool returnToStart = false;
    public bool following;
    private float timer = 0;
        // Use this for initialization
        void Start()
        {
        startPosition = gameObject.transform.position;
        startRotation = gameObject.transform.rotation;
    }

        // Update is called once per frame
        void Update()
        {


        


        if (move)
        {
            timer += Time.unscaledDeltaTime;

            transform.position = Vector3.Lerp(transform.position,    OtherCamera.transform.position, (timer / startTimer) * .05f);

            transform.rotation = Quaternion.Lerp(transform.rotation, OtherCamera.transform.rotation, (timer / startTimer) * .05f);
            if (timer / startTimer >= 1)

            {
            gameObject.GetComponent<PuzzleState>().StartPuzzle();
                timer = 0;
                following = true;
                move = false;
            }

        }
        if (following)
        {
            transform.position = OtherCamera.transform.position;
            transform.rotation = OtherCamera.transform.rotation;
        }

        if (returnToStart)
        {

            timer += Time.unscaledDeltaTime;

            transform.position = Vector3.Lerp(transform.position, startPosition, (timer / startTimer) * .05f);

            transform.rotation = Quaternion.Lerp(transform.rotation, startRotation, (timer / startTimer) * .05f);

            if (timer / startTimer >= 1)

            {
                gameObject.GetComponent<PuzzleState>().EndPuzzle();
                timer = 0;
               returnToStart = false;
            }

        }

    }
    public void Move( GameObject a_otherCamera)
    {
        OtherCamera = a_otherCamera;
        move = true;
    }
    public void Return()
    {
        following = false;
        returnToStart = true;
    }

}