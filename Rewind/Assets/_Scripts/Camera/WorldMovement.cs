﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class WorldMovement: MouseDrag{
    public float maxSpeed;
    public float minSpeed;
    public float maxYOffset;
    public float speedDropOff;
    public float minYMoveDist;
    protected float curSpeed;
    protected float slowTimer;
    protected float Yposition;
    protected Vector3 curPosition;
	// Use this for initialization
	void Start () {
        Yposition = transform.position.y;
        curPosition = transform.position;
	}

    // Update is called once per frame
    public override void Update() {
        
            
        if (isClicked)
        {


            curSpeed = -1 * (clickPosition.x - Input.mousePosition.x) /10;

            if (Mathf.Abs(curSpeed) > maxSpeed)
            {
                if (curSpeed < 0)
                    curSpeed = maxSpeed;
                else
                    curSpeed = -maxSpeed;
            }
            


            if (Mathf.Abs(clickPosition.y - Input.mousePosition.y) > minYMoveDist)
                curPosition.y -= -1 * (clickPosition.y - Input.mousePosition.y) *.05f *Time.unscaledDeltaTime;
            

            if (curPosition.y > maxYOffset + Yposition || curPosition.y < Yposition - maxYOffset)
                curPosition.y = (curPosition.y > maxYOffset + Yposition) ? Yposition + maxYOffset : Yposition - maxYOffset;

            transform.position = curPosition;

            if (Mathf.Abs(curSpeed) > 1)
                transform.Rotate(Vector3.up * curSpeed * Time.unscaledDeltaTime);
            

        }
        else
        {
            SpeedDropOff();
        }
        
        if (Input.GetMouseButtonUp(0) || Input.touchCount > 1)
            EndClick();
        
    }
    protected virtual void SpeedDropOff()
    {
        curSpeed *= speedDropOff;
        if (Mathf.Abs(curSpeed) > minSpeed)
            transform.Rotate(Vector3.up * curSpeed * Time.unscaledDeltaTime);
    }
}
