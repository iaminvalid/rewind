﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.ImageEffects;
public class VignetteControl : MonoBehaviour {
	public float BlurCornerMin;
	public float BlurCornerMax;
    public float BlurCornerDeviation;

    public float BlurDistMin;
	public float BlurDistMax;
    public float BlurDistDeviation;

    public float ChromaticMin;
	public float ChromaticMax;
    public float ChromaticDeviation;
	public float initialSpeed;

    private float BlurCornerValue;
    private float BlurDistValue  ;
    private float ChromaticValue ;

    private bool startShake;
    private bool chromaticForward;
    private bool startEffect;

    private TimeManager timeManager;
    private VignetteAndChromaticAberration vignette;


    
    // Use this for initialization
	void Start () {
        timeManager = GameObject.FindGameObjectWithTag("TimeManager").GetComponent<TimeManager>();
        vignette = GetComponent<VignetteAndChromaticAberration>();

        BlurCornerValue = BlurCornerMin;
        BlurDistValue =   BlurDistMin;
        ChromaticValue =  0;

    }
	
	// Update is called once per frame
	void Update () {
        if (startEffect)
        {
            if (!startShake)
            {
                ChromaticValue += ChromaticMin * Time.unscaledDeltaTime * initialSpeed;

                if (vignette.chromaticAberration >= ChromaticMin)
                {
                    startShake = true;
                }
            }
            else
            {
                if (chromaticForward)
                {
                    ChromaticValue += ChromaticDeviation * Time.unscaledDeltaTime;
                    if (ChromaticValue > ChromaticMax)
                    {
                        ChromaticValue = ChromaticMax;
                        chromaticForward = false;
                    }
                }
                else
                {
                    ChromaticValue -= ChromaticDeviation * Time.unscaledDeltaTime;
                    if (ChromaticValue < ChromaticMin)
                    {
                        ChromaticValue = ChromaticMin;
                        chromaticForward = true;
                    }
                }

            }
                BlurDistValue = Random.Range(BlurDistValue- BlurDistDeviation, BlurDistValue + BlurDistDeviation);
                BlurCornerValue = Random.Range(BlurCornerValue - BlurCornerDeviation, BlurCornerValue + BlurCornerDeviation);

            BlurDistValue = Mathf.Clamp(BlurDistValue, BlurDistMin, BlurDistMax);
            BlurCornerValue= Mathf.Clamp(BlurCornerValue, BlurCornerMin, BlurCornerMax);

			vignette.blurSpread = BlurDistValue;
			vignette.blur = BlurCornerValue;
			vignette.chromaticAberration = ChromaticValue;

        }


    }

	public void StartEffect()
    {
        startEffect = true;
    }

	public void EndEffect()
    {
		startEffect = false;
        startShake = false;

        ChromaticValue = 0;
        chromaticForward = true;
		vignette.blurSpread = 0;
		vignette.blur = 0;
		vignette.chromaticAberration = 0;
    }
}
