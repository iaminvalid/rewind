﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PinPointWorldMovement : WorldMovement
{
    private bool SlingRebound;
	private bool tutorialMode;
    private bool hasStartedYMovement;
    private bool hasMomentum;
    private float curSpeedXCap;
    private float curYPos;
    private Vector3 prevMousePos;
    private Quaternion originRot;

    public MovementTutorialPiece tutorialPiece;
    // Use this for initialization
    void Start()
    {
        Yposition = transform.position.y;
        curPosition = transform.position;
        originRot = transform.rotation;
    }
    
    // Update is called once per frame
    public override void Update()
    {
        ;

        if (hasMomentum)
            SpeedDropOff();

        if (isClicked)
        {
            XMovement();
            YMovement();


            if (Input.GetMouseButtonUp(0) || Input.touchCount >= 2 && !tutorialMode)
                EndClick();

            prevMousePos = Input.mousePosition;
        }
    }

        public override void StartClick()
    {
        if (tutorialMode)
            return;
        tutorialPiece.TutorialComplete();

        base.StartClick();
        prevMousePos = Input.mousePosition;
        originRot = transform.rotation;
    }
	public override void EndClick()
    {
        base.EndClick();
        if (Input.touchCount < 2)
        {
            curYPos = transform.position.y - Yposition ;
            curSpeed = prevMousePos.x - Input.mousePosition.x;
        }


        curSpeedXCap =  Mathf.Abs( curSpeed);

        //cur y position for next click and the rest of the calcuations

        hasMomentum = true;
        hasStartedYMovement = false;
        SpeedDropOff();
        //YSpeedDropOff();


    }
    protected override void SpeedDropOff()
    {
        //y rotation drop off (spinning left/right)
        transform.Rotate(new Vector3(0, -1, 0), curSpeed  * Time.unscaledDeltaTime);
        if (curSpeed < 0)
        {
            curSpeed += Time.unscaledDeltaTime * curSpeedXCap * speedDropOff;
            if (curSpeed > 0)
                curSpeed = 0;
            
        }
        else if (curSpeed > 0)
        {
            curSpeed -= Time.unscaledDeltaTime * curSpeedXCap * speedDropOff;
            if (curSpeed < 0)
            {
                curSpeed = 0;
            }
        }
       
    }
    private void XMovement()
    {
        Quaternion temp = originRot;
        float dist = clickPosition.x - Input.mousePosition.x;
        if (Mathf.Abs(dist) > 3  && Input.touchCount < 2) 
        {
            temp.eulerAngles = temp.eulerAngles + (new Vector3(0, -1, 0) * dist * 2* 0.01f); 
            transform.rotation = temp;
        }
    }
    private void YMovement()
    {
        
        Vector3 temp = transform.position;
        if (Mathf.Abs(clickPosition.y - Input.mousePosition.y) > minYMoveDist || hasStartedYMovement)
        {
            if (!hasStartedYMovement)
                hasStartedYMovement = true;

            temp = curPosition + new Vector3(0,(clickPosition.y - Input.mousePosition.y) * 0.01f + curYPos, 0);
        }
        if (temp.y > maxYOffset + Yposition || temp.y < Yposition - maxYOffset )
            temp.y= (temp.y > maxYOffset + Yposition ) ? Yposition + maxYOffset  : Yposition - maxYOffset ;
        


        transform.position = temp;
    }

    private void YSpeedDropOff()
    {
    }
	public void SetTutorialMode(bool value)
	{
		tutorialMode = value;

	}

}
