﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseDrag : MonoBehaviour {

    public bool isClicked;
    protected Vector3 clickPosition;
    protected Vector3 originPosition;
    void Start()
    {
        originPosition = transform.position;
    }
    // Update is called once per frame
    virtual public void Update () {
        if (isClicked)
        {
            Vector3 mousePos;
            if (Input.touchCount >=2)
                mousePos = Input.GetTouch(0).position;
            else
                mousePos = Input.mousePosition;

            Vector3 camSpacePos = Camera.main.WorldToScreenPoint(transform.position);
            mousePos.z = camSpacePos.z;

            Vector3 tempPos = mousePos;
            tempPos = Camera.main.ScreenToWorldPoint(tempPos);


            transform.position = Vector3.Lerp(transform.position, tempPos, Time.deltaTime);



            if (Input.GetMouseButtonUp(0) || Input.touchCount >= 2)
                EndClick();
        }
        
	}
    public virtual void StartClick()
    {
        clickPosition = Input.mousePosition;
        originPosition = transform.position;
        isClicked = true;


    }
	public virtual void EndClick()
    {
        isClicked = false;
        if (GetComponent<Rigidbody>())
            GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;

    }
}
